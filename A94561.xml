<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A94561">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the memory of Thomas Heneage Esquire. An expostulation with death.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A94561 of text R2571 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason E149_10). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A94561</idno>
    <idno type="STC">Wing T1572</idno>
    <idno type="STC">Thomason E149_10</idno>
    <idno type="STC">ESTC R2571</idno>
    <idno type="EEBO-CITATION">99872100</idno>
    <idno type="PROQUEST">99872100</idno>
    <idno type="VID">156718</idno>
    <idno type="PROQUESTGOID">2248513106</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A94561)</note>
    <note>Transcribed from: (Early English Books Online ; image set 156718)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 26:E149[10])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the memory of Thomas Heneage Esquire. An expostulation with death.</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>printed for Edward Blackmore,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1642.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>In verse.</note>
      <note>In double columns.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Heneage, Thomas, d. 1642?</term>
     <term>Death -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A94561</ep:tcp>
    <ep:estc> R2571</ep:estc>
    <ep:stc> (Thomason E149_10). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>To the memory of Thomas Heneage Esquire.:  An expostulation with death.</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1642</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>337</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-06</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-07</date><label>Pip Willcox</label>
        Sampled and proofread
      </change>
   <change><date>2007-07</date><label>Pip Willcox</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A94561-e10">
  <body xml:id="A94561-e20">
   <pb facs="tcp:156718:1" rend="simple:additions" xml:id="A94561-001-a"/>
   <div type="elegy" xml:id="A94561-e30">
    <head xml:id="A94561-e40">
     <w lemma="to" pos="acp" xml:id="A94561-001-a-0010">TO</w>
     <w lemma="the" pos="d" xml:id="A94561-001-a-0020">THE</w>
     <w lemma="memory" pos="n1" xml:id="A94561-001-a-0030">MEMORY</w>
     <w lemma="of" pos="acp" xml:id="A94561-001-a-0040">OF</w>
     <w lemma="THOMAS" pos="nn1" xml:id="A94561-001-a-0050">THOMAS</w>
     <w lemma="Heneage" pos="nn1" xml:id="A94561-001-a-0060">HENEAGE</w>
     <w lemma="esquire" pos="n1" xml:id="A94561-001-a-0070">ESQUIRE</w>
     <pc unit="sentence" xml:id="A94561-001-a-0080">.</pc>
    </head>
    <head type="sub" xml:id="A94561-e50">
     <w lemma="a" pos="d" xml:id="A94561-001-a-0090">An</w>
     <w lemma="expostulation" pos="n1" xml:id="A94561-001-a-0100">Expostulation</w>
     <w lemma="with" pos="acp" xml:id="A94561-001-a-0110">with</w>
     <w lemma="death" pos="n1" xml:id="A94561-001-a-0120">DEATH</w>
     <pc unit="sentence" xml:id="A94561-001-a-0130">.</pc>
    </head>
    <lg xml:id="A94561-e60">
     <l xml:id="A94561-e70">
      <w lemma="be" pos="vvz" xml:id="A94561-001-a-0140">IS</w>
      <w lemma="there" pos="av" xml:id="A94561-001-a-0150">there</w>
      <w lemma="no" pos="dx" xml:id="A94561-001-a-0160">no</w>
      <w lemma="remedy" pos="n1" xml:id="A94561-001-a-0170">remedy</w>
      <pc xml:id="A94561-001-a-0180">,</pc>
      <w lemma="must" pos="vmb" xml:id="A94561-001-a-0190">must</w>
      <w lemma="wise" pos="j" xml:id="A94561-001-a-0200">wise</w>
      <w lemma="man" pos="n2" xml:id="A94561-001-a-0210">men</w>
      <w lemma="die" pos="vvb" reg="die" xml:id="A94561-001-a-0220">dy</w>
     </l>
     <l xml:id="A94561-e80">
      <w lemma="as" pos="acp" xml:id="A94561-001-a-0230">As</w>
      <w lemma="well" pos="av" xml:id="A94561-001-a-0240">well</w>
      <w lemma="as" pos="acp" xml:id="A94561-001-a-0250">as</w>
      <w lemma="fool" pos="n2" reg="fools" xml:id="A94561-001-a-0260">fooles</w>
      <pc xml:id="A94561-001-a-0270">?</pc>
      <w lemma="have" pos="vvz" xml:id="A94561-001-a-0280">hath</w>
      <w lemma="perfect" pos="j" xml:id="A94561-001-a-0290">perfect</w>
      <w lemma="piety" pos="n1" xml:id="A94561-001-a-0300">Piety</w>
     </l>
     <l xml:id="A94561-e90">
      <w lemma="no" pos="dx" xml:id="A94561-001-a-0310">No</w>
      <w lemma="privilege" pos="n1" reg="privilege" xml:id="A94561-001-a-0320">priviledge</w>
      <w lemma="beyond" pos="acp" xml:id="A94561-001-a-0330">beyond</w>
      <w lemma="impiety" pos="n1" xml:id="A94561-001-a-0340">Impiety</w>
      <pc unit="sentence" xml:id="A94561-001-a-0350">?</pc>
     </l>
     <l xml:id="A94561-e100">
      <w lemma="impartial" pos="j" reg="Impartial" xml:id="A94561-001-a-0360">Impartiall</w>
      <w lemma="death" pos="n1" xml:id="A94561-001-a-0370">Death</w>
      <pc xml:id="A94561-001-a-0380">,</pc>
      <w lemma="when" pos="crq" xml:id="A94561-001-a-0390">when</w>
      <w lemma="will" pos="vmb" xml:id="A94561-001-a-0400">will</w>
      <w lemma="thy" pos="po" xml:id="A94561-001-a-0410">thy</w>
      <w lemma="poison" pos="j-vn" reg="poisoned" xml:id="A94561-001-a-0420">poysoned</w>
      <w lemma="dart" pos="n1" xml:id="A94561-001-a-0430">dart</w>
     </l>
     <l xml:id="A94561-e110">
      <w lemma="learn" pos="vvb" reg="Learn" xml:id="A94561-001-a-0440">Learne</w>
      <w lemma="to" pos="prt" xml:id="A94561-001-a-0450">to</w>
      <w lemma="distinguish" pos="vvi" xml:id="A94561-001-a-0460">distinguish</w>
      <w lemma="betwixt" pos="acp" reg="twixt" xml:id="A94561-001-a-0470">'twixt</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-0480">the</w>
      <w lemma="noble" pos="jc" xml:id="A94561-001-a-0490">Nobler</w>
      <w lemma="part" pos="n1" xml:id="A94561-001-a-0500">part</w>
     </l>
     <l xml:id="A94561-e120">
      <w lemma="of" pos="acp" xml:id="A94561-001-a-0510">Of</w>
      <w lemma="live" pos="j-vg" xml:id="A94561-001-a-0520">living</w>
      <w lemma="man" pos="n2" xml:id="A94561-001-a-0530">men</w>
      <pc xml:id="A94561-001-a-0540">,</pc>
      <w lemma="and" pos="cc" xml:id="A94561-001-a-0550">and</w>
      <w lemma="those" pos="d" xml:id="A94561-001-a-0560">those</w>
      <w lemma="who" pos="crq" xml:id="A94561-001-a-0570">whose</w>
      <w lemma="vulgar" pos="j" xml:id="A94561-001-a-0580">vulgar</w>
      <w lemma="breath" pos="n1" xml:id="A94561-001-a-0590">breath</w>
     </l>
     <l xml:id="A94561-e130">
      <w lemma="be" pos="vvd" xml:id="A94561-001-a-0600">Were</w>
      <w lemma="far" pos="av-j" reg="far" xml:id="A94561-001-a-0610">farre</w>
      <w lemma="a" pos="d" xml:id="A94561-001-a-0620">a</w>
      <w lemma="fit" pos="jc" xml:id="A94561-001-a-0630">fitter</w>
      <w lemma="sacrifice" pos="n1" xml:id="A94561-001-a-0640">sacrifice</w>
      <w lemma="for" pos="acp" xml:id="A94561-001-a-0650">for</w>
      <w lemma="death" pos="n1" xml:id="A94561-001-a-0660">death</w>
      <pc unit="sentence" xml:id="A94561-001-a-0670">?</pc>
     </l>
     <l xml:id="A94561-e140">
      <w lemma="why" pos="crq" xml:id="A94561-001-a-0680">Why</w>
      <w lemma="be" pos="vvb" xml:id="A94561-001-a-0690">are</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-0700">the</w>
      <w lemma="star" pos="n2" xml:id="A94561-001-a-0710">Stars</w>
      <w lemma="immortal" pos="j" reg="immortal" xml:id="A94561-001-a-0720">immortall</w>
      <w lemma="more" pos="avc-d" xml:id="A94561-001-a-0730">more</w>
      <w lemma="than" pos="cs" xml:id="A94561-001-a-0740">than</w>
      <w lemma="they" pos="pns" xml:id="A94561-001-a-0750">they</w>
      <pc unit="sentence" xml:id="A94561-001-a-0760">?</pc>
     </l>
     <l xml:id="A94561-e150">
      <w lemma="they" pos="pns" xml:id="A94561-001-a-0770">They</w>
      <w lemma="shine" pos="vvb" xml:id="A94561-001-a-0780">shine</w>
      <w lemma="by" pos="acp" xml:id="A94561-001-a-0790">by</w>
      <w lemma="night" pos="n1" xml:id="A94561-001-a-0800">night</w>
      <pc xml:id="A94561-001-a-0810">,</pc>
      <w lemma="these" pos="d" xml:id="A94561-001-a-0820">these</w>
      <w lemma="glister" pos="n1" xml:id="A94561-001-a-0830">glister</w>
      <w lemma="night" pos="n1" xml:id="A94561-001-a-0840">night</w>
      <w lemma="and" pos="cc" xml:id="A94561-001-a-0850">and</w>
      <w lemma="day" pos="n1" xml:id="A94561-001-a-0860">day</w>
      <pc unit="sentence" xml:id="A94561-001-a-0870">.</pc>
     </l>
     <l xml:id="A94561-e160">
      <w lemma="star" pos="n2" reg="Stars" xml:id="A94561-001-a-0880">Starres</w>
      <w lemma="borrow" pos="vvb" xml:id="A94561-001-a-0890">borrow</w>
      <w lemma="all" pos="d" xml:id="A94561-001-a-0900">all</w>
      <w lemma="their" pos="po" xml:id="A94561-001-a-0910">their</w>
      <w lemma="light" pos="n1" xml:id="A94561-001-a-0920">light</w>
      <pc xml:id="A94561-001-a-0930">,</pc>
      <w lemma="but" pos="acp" xml:id="A94561-001-a-0940">but</w>
      <w lemma="wiseman" pos="n2" xml:id="A94561-001-a-0950">wisemen</w>
      <w lemma="lend" pos="vvi" xml:id="A94561-001-a-0960">lend</w>
      <pc xml:id="A94561-001-a-0970">:</pc>
     </l>
     <l xml:id="A94561-e170">
      <w lemma="in" pos="acp" xml:id="A94561-001-a-0980">In</w>
      <w lemma="each" pos="d" xml:id="A94561-001-a-0990">each</w>
      <w lemma="of" pos="acp" xml:id="A94561-001-a-1000">of</w>
      <w lemma="they" pos="pno" xml:id="A94561-001-a-1010">them</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-1020">the</w>
      <w lemma="world" pos="n1" xml:id="A94561-001-a-1030">world</w>
      <w lemma="enjoy" pos="vvz" reg="enjoys" xml:id="A94561-001-a-1040">enjoyes</w>
      <w lemma="a" pos="d" xml:id="A94561-001-a-1050">a</w>
      <w lemma="friend" pos="n1" xml:id="A94561-001-a-1060">friend</w>
      <pc unit="sentence" xml:id="A94561-001-a-1070">.</pc>
     </l>
     <l xml:id="A94561-e180">
      <w lemma="how" pos="crq" xml:id="A94561-001-a-1080">How</w>
      <w lemma="be" pos="vvz" xml:id="A94561-001-a-1090">is</w>
      <w lemma="it" pos="pn" xml:id="A94561-001-a-1100">it</w>
      <w lemma="then" pos="av" xml:id="A94561-001-a-1110">then</w>
      <w lemma="that" pos="d" xml:id="A94561-001-a-1120">that</w>
      <w lemma="senseless" pos="j" reg="senseless" xml:id="A94561-001-a-1130">senslesse</w>
      <w lemma="creature" pos="n2" xml:id="A94561-001-a-1140">creatures</w>
      <w lemma="be" pos="vvb" xml:id="A94561-001-a-1150">be</w>
     </l>
     <l xml:id="A94561-e190">
      <w lemma="exempt" pos="vvn" xml:id="A94561-001-a-1160">Exempted</w>
      <w lemma="from" pos="acp" xml:id="A94561-001-a-1170">from</w>
      <w lemma="this" pos="d" xml:id="A94561-001-a-1180">this</w>
      <w lemma="general" pos="j" reg="general" xml:id="A94561-001-a-1190">generall</w>
      <w lemma="tyranny" pos="n1" xml:id="A94561-001-a-1200">tyranny</w>
      <pc xml:id="A94561-001-a-1210">,</pc>
     </l>
     <l xml:id="A94561-e200">
      <w lemma="and" pos="cc" xml:id="A94561-001-a-1220">And</w>
      <w lemma="fix" pos="vvn" reg="fixed" xml:id="A94561-001-a-1230">fixt</w>
      <w lemma="within" pos="acp" xml:id="A94561-001-a-1240">within</w>
      <w lemma="their" pos="po" xml:id="A94561-001-a-1250">their</w>
      <w lemma="orb" pos="n2" xml:id="A94561-001-a-1260">Orbs</w>
      <pc xml:id="A94561-001-a-1270">,</pc>
      <w lemma="survive" pos="vvb" xml:id="A94561-001-a-1280">survive</w>
      <w lemma="to" pos="prt" xml:id="A94561-001-a-1290">to</w>
      <w lemma="light" pos="vvi" xml:id="A94561-001-a-1300">light</w>
     </l>
     <l xml:id="A94561-e210">
      <w lemma="so" pos="av" xml:id="A94561-001-a-1310">So</w>
      <w lemma="many" pos="d" xml:id="A94561-001-a-1320">many</w>
      <w lemma="worthy" pos="n2-j" xml:id="A94561-001-a-1330">Worthies</w>
      <w lemma="to" pos="acp" xml:id="A94561-001-a-1340">to</w>
      <w lemma="eternal" pos="j" reg="eternal" xml:id="A94561-001-a-1350">eternall</w>
      <w lemma="night" pos="n1" xml:id="A94561-001-a-1360">night</w>
      <pc unit="sentence" xml:id="A94561-001-a-1370">?</pc>
     </l>
     <l xml:id="A94561-e220">
      <w lemma="be" pos="vvz" xml:id="A94561-001-a-1380">Is</w>
      <w lemma="it" pos="pn" xml:id="A94561-001-a-1390">it</w>
      <w lemma="because" pos="acp" xml:id="A94561-001-a-1400">because</w>
      <w lemma="star" pos="n2" xml:id="A94561-001-a-1410">Stars</w>
      <w lemma="be" pos="vvb" xml:id="A94561-001-a-1420">are</w>
      <w lemma="above" pos="acp" xml:id="A94561-001-a-1430">above</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-1440">the</w>
      <w lemma="sphere" pos="n1" reg="sphere" xml:id="A94561-001-a-1450">Spheere</w>
      <pc xml:id="A94561-001-a-1460">,</pc>
     </l>
     <l xml:id="A94561-e230">
      <w lemma="wherein" pos="crq" xml:id="A94561-001-a-1470">Wherein</w>
      <w lemma="thou" pos="pns" xml:id="A94561-001-a-1480">thou</w>
      <w lemma="be" pos="vv2" xml:id="A94561-001-a-1490">art</w>
      <w lemma="allow" pos="vvn" reg="allowed" xml:id="A94561-001-a-1500">allow'd</w>
      <w lemma="to" pos="prt" xml:id="A94561-001-a-1510">to</w>
      <w lemma="domineer" pos="vvi" reg="domineer" xml:id="A94561-001-a-1520">domineere</w>
      <pc unit="sentence" xml:id="A94561-001-a-1530">?</pc>
     </l>
     <l xml:id="A94561-e240">
      <w lemma="or" pos="cc" xml:id="A94561-001-a-1540">Or</w>
      <w lemma="be" pos="vvz" xml:id="A94561-001-a-1550">is</w>
      <w lemma="thy" pos="po" xml:id="A94561-001-a-1560">thy</w>
      <w lemma="arm" pos="n1" reg="arm" xml:id="A94561-001-a-1570">arme</w>
      <w lemma="too" pos="av" xml:id="A94561-001-a-1580">too</w>
      <w lemma="short" pos="j" xml:id="A94561-001-a-1590">short</w>
      <pc xml:id="A94561-001-a-1600">,</pc>
      <w lemma="or" pos="cc" xml:id="A94561-001-a-1610">or</w>
      <w lemma="will" pos="vm2" xml:id="A94561-001-a-1620">wilt</w>
      <w lemma="thou" pos="pns" xml:id="A94561-001-a-1630">thou</w>
      <w lemma="say" pos="vvi" xml:id="A94561-001-a-1640">say</w>
     </l>
     <l xml:id="A94561-e250">
      <w lemma="the" pos="d" xml:id="A94561-001-a-1650">The</w>
      <w lemma="night" pos="n1" xml:id="A94561-001-a-1660">night</w>
      <w lemma="revive" pos="vvz" xml:id="A94561-001-a-1670">revives</w>
      <w lemma="they" pos="pno" xml:id="A94561-001-a-1680">them</w>
      <w lemma="that" pos="cs" xml:id="A94561-001-a-1690">that</w>
      <w lemma="depart" pos="vvb" xml:id="A94561-001-a-1700">depart</w>
      <w lemma="by" pos="acp" xml:id="A94561-001-a-1710">by</w>
      <w lemma="day" pos="n1" xml:id="A94561-001-a-1720">day</w>
      <pc xml:id="A94561-001-a-1730">;</pc>
     </l>
     <l xml:id="A94561-e260">
      <w lemma="that" pos="cs" xml:id="A94561-001-a-1740">That</w>
      <w lemma="every" pos="d" xml:id="A94561-001-a-1750">every</w>
      <w lemma="even" pos="av" xml:id="A94561-001-a-1760">Even</w>
      <w lemma="behold" pos="vvz" xml:id="A94561-001-a-1770">beholds</w>
      <w lemma="a" pos="d" xml:id="A94561-001-a-1780">a</w>
      <w lemma="die" pos="j-vg" xml:id="A94561-001-a-1790">dying</w>
      <w lemma="sun" pos="n1" xml:id="A94561-001-a-1800">Sun</w>
      <pc xml:id="A94561-001-a-1810">,</pc>
     </l>
     <l xml:id="A94561-e270">
      <w lemma="and" pos="cc" xml:id="A94561-001-a-1820">And</w>
      <w lemma="every" pos="d" xml:id="A94561-001-a-1830">every</w>
      <w lemma="morn" pos="n1" xml:id="A94561-001-a-1840">Morn</w>
      <w lemma="a" pos="d" xml:id="A94561-001-a-1850">a</w>
      <w lemma="resurrection" pos="n1" xml:id="A94561-001-a-1860">Resurrection</w>
      <pc unit="sentence" xml:id="A94561-001-a-1870">?</pc>
     </l>
     <l xml:id="A94561-e280">
      <w lemma="oh" pos="uh" xml:id="A94561-001-a-1880">Oh</w>
      <w lemma="happy" pos="j" xml:id="A94561-001-a-1890">happy</w>
      <w lemma="world" pos="n1" xml:id="A94561-001-a-1900">world</w>
      <pc xml:id="A94561-001-a-1910">,</pc>
      <w lemma="if" pos="cs" xml:id="A94561-001-a-1920">if</w>
      <w lemma="wise" pos="j" xml:id="A94561-001-a-1930">wise</w>
      <w lemma="and" pos="cc" xml:id="A94561-001-a-1940">and</w>
      <w lemma="virtuous" pos="j" reg="virtuous" xml:id="A94561-001-a-1950">vertuous</w>
      <w lemma="man" pos="n2" xml:id="A94561-001-a-1960">men</w>
      <pc xml:id="A94561-001-a-1970">,</pc>
     </l>
     <l xml:id="A94561-e290">
      <w lemma="since" pos="acp" xml:id="A94561-001-a-1980">Since</w>
      <w lemma="they" pos="pns" xml:id="A94561-001-a-1990">they</w>
      <w lemma="must" pos="vmb" xml:id="A94561-001-a-2000">must</w>
      <w lemma="die" pos="vvi" reg="die" xml:id="A94561-001-a-2010">dy</w>
      <pc xml:id="A94561-001-a-2020">,</pc>
      <w lemma="may" pos="vmd" xml:id="A94561-001-a-2030">might</w>
      <w lemma="die" pos="vvi" reg="die" xml:id="A94561-001-a-2040">dy</w>
      <w lemma="to" pos="prt" xml:id="A94561-001-a-2050">to</w>
      <w lemma="live" pos="vvi" xml:id="A94561-001-a-2060">live</w>
      <w lemma="again" pos="av" reg="again" xml:id="A94561-001-a-2070">agen</w>
     </l>
     <l xml:id="A94561-e300">
      <w lemma="after" pos="acp" xml:id="A94561-001-a-2080">After</w>
      <w lemma="some" pos="d" xml:id="A94561-001-a-2090">some</w>
      <w lemma="hour" pos="n2" reg="hours" xml:id="A94561-001-a-2100">houres</w>
      <w lemma="like" pos="acp" xml:id="A94561-001-a-2110">like</w>
      <w lemma="Sol" pos="fla" xml:id="A94561-001-a-2120">Sol</w>
      <pc xml:id="A94561-001-a-2130">,</pc>
      <w lemma="or" pos="cc" xml:id="A94561-001-a-2140">or</w>
      <w lemma="with" pos="acp" xml:id="A94561-001-a-2150">with</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-2160">the</w>
      <w lemma="moon" pos="n1" xml:id="A94561-001-a-2170">Moon</w>
     </l>
     <l xml:id="A94561-e310">
      <w lemma="after" pos="acp" xml:id="A94561-001-a-2180">After</w>
      <w lemma="some" pos="d" xml:id="A94561-001-a-2190">some</w>
      <w lemma="day" pos="n2" reg="days" xml:id="A94561-001-a-2200">dayes</w>
      <pc xml:id="A94561-001-a-2210">;</pc>
      <w lemma="if" pos="cs" xml:id="A94561-001-a-2220">if</w>
      <w lemma="that" pos="d" xml:id="A94561-001-a-2230">that</w>
      <w lemma="be" pos="vvb" xml:id="A94561-001-a-2240">be</w>
      <w lemma="think" pos="vvn" xml:id="A94561-001-a-2250">thought</w>
      <w lemma="too" pos="av" xml:id="A94561-001-a-2260">too</w>
      <w lemma="soon" pos="av" xml:id="A94561-001-a-2270">soon</w>
      <pc xml:id="A94561-001-a-2280">,</pc>
     </l>
     <l xml:id="A94561-e320">
      <w lemma="that" pos="cs" xml:id="A94561-001-a-2290">That</w>
      <w lemma="like" pos="acp" xml:id="A94561-001-a-2300">like</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-2310">the</w>
      <w lemma="fragrant" pos="j" xml:id="A94561-001-a-2320">fragrant</w>
      <w lemma="flower" pos="n2" xml:id="A94561-001-a-2330">flowers</w>
      <w lemma="they" pos="pns" xml:id="A94561-001-a-2340">they</w>
      <w lemma="may" pos="vmd" xml:id="A94561-001-a-2350">might</w>
      <w lemma="appear" pos="vvi" xml:id="A94561-001-a-2360">appear</w>
     </l>
     <l xml:id="A94561-e330">
      <w lemma="to" pos="prt" xml:id="A94561-001-a-2370">To</w>
      <w lemma="beautify" pos="vvi" reg="beautify" xml:id="A94561-001-a-2380">beautifie</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-2390">the</w>
      <w lemma="earth" pos="n1" xml:id="A94561-001-a-2400">Earth</w>
      <w lemma="but" pos="acp" xml:id="A94561-001-a-2410">but</w>
      <w lemma="once" pos="acp" xml:id="A94561-001-a-2420">once</w>
      <w lemma="a" pos="d" xml:id="A94561-001-a-2430">a</w>
      <w lemma="year" pos="n1" reg="year" xml:id="A94561-001-a-2440">yeare</w>
      <pc xml:id="A94561-001-a-2450">;</pc>
     </l>
     <l xml:id="A94561-e340">
      <w lemma="then" pos="av" xml:id="A94561-001-a-2460">Then</w>
      <w lemma="happy" pos="av-j" xml:id="A94561-001-a-2470">happily</w>
      <w lemma="we" pos="pns" xml:id="A94561-001-a-2480">we</w>
      <w lemma="may" pos="vmd" xml:id="A94561-001-a-2490">might</w>
      <w lemma="ourselves" pos="pr" reg="ourselves" xml:id="A94561-001-a-2500">our selves</w>
      <w lemma="enure" pos="vvi" reg="enure" xml:id="A94561-001-a-2520">inure</w>
     </l>
     <l xml:id="A94561-e350">
      <w lemma="his" pos="po" xml:id="A94561-001-a-2530">His</w>
      <w lemma="death" pos="n1" xml:id="A94561-001-a-2540">death</w>
      <w lemma="with" pos="acp" xml:id="A94561-001-a-2550">with</w>
      <w lemma="some" pos="d" xml:id="A94561-001-a-2560">some</w>
      <w lemma="more" pos="avc-d" xml:id="A94561-001-a-2570">more</w>
      <w lemma="patience" pos="n1" xml:id="A94561-001-a-2580">patience</w>
      <w lemma="to" pos="prt" xml:id="A94561-001-a-2590">to</w>
      <w lemma="endure" pos="vvi" xml:id="A94561-001-a-2600">endure</w>
      <pc xml:id="A94561-001-a-2610">,</pc>
     </l>
     <l xml:id="A94561-e360">
      <w lemma="who" pos="crq" xml:id="A94561-001-a-2620">Whose</w>
      <w lemma="grace" pos="n1" xml:id="A94561-001-a-2630">grace</w>
      <w lemma="and" pos="cc" xml:id="A94561-001-a-2640">and</w>
      <w lemma="wisdom" pos="n1" reg="wisdom" xml:id="A94561-001-a-2650">wisedome</w>
      <w lemma="do" pos="vvd" xml:id="A94561-001-a-2660">did</w>
      <w lemma="trascend" pos="vvi" xml:id="A94561-001-a-2670">trascend</w>
      <w lemma="by" pos="acp" xml:id="A94561-001-a-2680">by</w>
      <w lemma="far" pos="av-j" reg="far" xml:id="A94561-001-a-2690">farre</w>
     </l>
     <l xml:id="A94561-e370">
      <w lemma="the" pos="d" xml:id="A94561-001-a-2700">The</w>
      <w lemma="light" pos="n1" xml:id="A94561-001-a-2710">light</w>
      <w lemma="and" pos="cc" xml:id="A94561-001-a-2720">and</w>
      <w lemma="influence" pos="n1" xml:id="A94561-001-a-2730">influence</w>
      <w lemma="of" pos="acp" xml:id="A94561-001-a-2740">of</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-2750">the</w>
      <w lemma="morning" pos="n1" xml:id="A94561-001-a-2760">Morning</w>
      <w lemma="star" pos="n1" xml:id="A94561-001-a-2770">Star</w>
      <pc unit="sentence" xml:id="A94561-001-a-2780">.</pc>
     </l>
     <l xml:id="A94561-e380">
      <w lemma="but" pos="acp" xml:id="A94561-001-a-2790">But</w>
      <w lemma="this" pos="d" xml:id="A94561-001-a-2800">this</w>
      <w lemma="vain" pos="j" reg="vain" xml:id="A94561-001-a-2810">vaine</w>
      <w lemma="wish" pos="n1" xml:id="A94561-001-a-2820">wish</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-2830">the</w>
      <w lemma="great" pos="j" xml:id="A94561-001-a-2840">Great</w>
      <w lemma="and" pos="cc" xml:id="A94561-001-a-2850">and</w>
      <w lemma="only" pos="av-j" xml:id="A94561-001-a-2860">only</w>
      <w lemma="wise" pos="j" xml:id="A94561-001-a-2870">Wise</w>
     </l>
     <l xml:id="A94561-e390">
      <w lemma="controller" pos="n1" xml:id="A94561-001-a-2880">Controller</w>
      <w lemma="of" pos="acp" xml:id="A94561-001-a-2890">of</w>
      <w lemma="the" pos="d" xml:id="A94561-001-a-2900">the</w>
      <w lemma="universe" pos="n1" reg="universe" xml:id="A94561-001-a-2910">Vniverse</w>
      <w lemma="deny" pos="vvz" xml:id="A94561-001-a-2920">denies</w>
      <pc xml:id="A94561-001-a-2930">,</pc>
     </l>
     <l xml:id="A94561-e400">
      <w lemma="and" pos="cc" xml:id="A94561-001-a-2940">And</w>
      <w join="right" lemma="it" pos="pn" xml:id="A94561-001-a-2950">'t</w>
      <w join="left" lemma="will" pos="vmb" xml:id="A94561-001-a-2951">will</w>
      <w lemma="become" pos="vvi" xml:id="A94561-001-a-2960">become</w>
      <w lemma="we" pos="pno" xml:id="A94561-001-a-2970">us</w>
      <w lemma="better" pos="avc-j" xml:id="A94561-001-a-2980">better</w>
      <pc xml:id="A94561-001-a-2990">,</pc>
      <w lemma="reverent" pos="av-j" xml:id="A94561-001-a-3000">reverently</w>
     </l>
     <l xml:id="A94561-e410">
      <w lemma="to" pos="prt" xml:id="A94561-001-a-3010">To</w>
      <w lemma="mourn" pos="vvi" reg="mourn" xml:id="A94561-001-a-3020">mourne</w>
      <w lemma="in" pos="acp" xml:id="A94561-001-a-3030">in</w>
      <w lemma="silence" pos="n1" xml:id="A94561-001-a-3040">silence</w>
      <pc xml:id="A94561-001-a-3050">,</pc>
      <w lemma="rather" pos="avc" xml:id="A94561-001-a-3060">rather</w>
      <w lemma="than" pos="cs" xml:id="A94561-001-a-3070">than</w>
      <w lemma="reply" pos="vvi" xml:id="A94561-001-a-3080">reply</w>
      <pc unit="sentence" xml:id="A94561-001-a-3090">.</pc>
     </l>
    </lg>
    <div type="epitaph" xml:id="A94561-e420">
     <head xml:id="A94561-e430">
      <w lemma="the" pos="d" xml:id="A94561-001-a-3100">The</w>
      <w lemma="epitaph" pos="n1" xml:id="A94561-001-a-3110">Epitaph</w>
      <pc unit="sentence" xml:id="A94561-001-a-3120">.</pc>
     </head>
     <lg xml:id="A94561-e440">
      <l xml:id="A94561-e450">
       <w lemma="here" pos="av" xml:id="A94561-001-a-3130">HEre</w>
       <w lemma="lie" pos="vvz" xml:id="A94561-001-a-3140">lies</w>
       <w lemma="inter" pos="vvn" reg="interred" xml:id="A94561-001-a-3150">interr'd</w>
       <w lemma="under" pos="acp" xml:id="A94561-001-a-3160">under</w>
       <w lemma="this" pos="d" xml:id="A94561-001-a-3170">this</w>
       <w lemma="fat" pos="j" xml:id="A94561-001-a-3180">fat</w>
       <w lemma="all" pos="d" xml:id="A94561-001-a-3190">all</w>
       <w lemma="stone" pos="n1" xml:id="A94561-001-a-3200">Stone</w>
      </l>
      <l xml:id="A94561-e460">
       <w lemma="a" pos="d" xml:id="A94561-001-a-3210">A</w>
       <w lemma="world" pos="n1" xml:id="A94561-001-a-3220">world</w>
       <w lemma="of" pos="acp" xml:id="A94561-001-a-3230">of</w>
       <w lemma="man" pos="n2" xml:id="A94561-001-a-3240">men</w>
       <w lemma="epitomise" pos="vvn" reg="epitomised" xml:id="A94561-001-a-3250">epitomis'd</w>
       <w lemma="in" pos="acp" xml:id="A94561-001-a-3260">in</w>
       <w lemma="one" pos="pi" xml:id="A94561-001-a-3270">one</w>
       <pc unit="sentence" xml:id="A94561-001-a-3280">.</pc>
      </l>
     </lg>
    </div>
   </div>
  </body>
  <back xml:id="A94561-e470">
   <div type="colophon" xml:id="A94561-e480">
    <p xml:id="A94561-e490">
     <w lemma="london" pos="nn1" xml:id="A94561-001-a-3290">London</w>
     <pc xml:id="A94561-001-a-3300">,</pc>
     <w lemma="print" pos="vvn" xml:id="A94561-001-a-3310">printed</w>
     <w lemma="for" pos="acp" xml:id="A94561-001-a-3320">for</w>
     <hi xml:id="A94561-e500">
      <w lemma="Edward" pos="nn1" xml:id="A94561-001-a-3330">Edward</w>
      <w lemma="Blackmore" pos="nn1" xml:id="A94561-001-a-3340">Blackmore</w>
      <pc xml:id="A94561-001-a-3350">,</pc>
     </hi>
     <w lemma="1642." pos="crd" xml:id="A94561-001-a-3360">1642.</w>
     <pc unit="sentence" xml:id="A94561-001-a-3370"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
