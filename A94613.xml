<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A94613">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the right honourable the House of Peers, now assembled in Parliament. The humble petition of Knights, gentlemen, freeholders, and others inhabitants of the county of Kent.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A94613 of text R210278 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.4[58]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A94613</idno>
    <idno type="STC">Wing T1627</idno>
    <idno type="STC">Thomason 669.f.4[58]</idno>
    <idno type="STC">ESTC R210278</idno>
    <idno type="EEBO-CITATION">99869090</idno>
    <idno type="PROQUEST">99869090</idno>
    <idno type="VID">160680</idno>
    <idno type="PROQUESTGOID">2248506012</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A94613)</note>
    <note>Transcribed from: (Early English Books Online ; image set 160680)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 245:669f4[58])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the right honourable the House of Peers, now assembled in Parliament. The humble petition of Knights, gentlemen, freeholders, and others inhabitants of the county of Kent.</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed by R. Oulten and G. Dexter,</publisher>
      <pubPlace>London :</pubPlace>
      <date>1641.</date>
     </publicationStmt>
     <notesStmt>
      <note>With engraved border.</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament. -- House of Lords -- Early works to 1800.</term>
     <term>Church of England -- Bishops -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A94613</ep:tcp>
    <ep:estc> R210278</ep:estc>
    <ep:stc> (Thomason 669.f.4[58]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>To the right honourable the House of Peers, now assembled in Parliament. The humble petition of Knights, gentlemen, freeholders, and others</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1641</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>348</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-06</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-07</date><label>Robyn Anspach</label>
        Sampled and proofread
      </change>
   <change><date>2007-07</date><label>Robyn Anspach</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A94613-e10">
  <body xml:id="A94613-e20">
   <pb facs="tcp:160680:1" rend="simple:additions" xml:id="A94613-001-a"/>
   <div type="text" xml:id="A94613-e30">
    <head xml:id="A94613-e40">
     <w lemma="to" pos="acp" xml:id="A94613-001-a-0010">TO</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0020">THE</w>
     <w lemma="right" pos="n1-j" xml:id="A94613-001-a-0030">RIGHT</w>
     <w lemma="honourable" pos="j" xml:id="A94613-001-a-0040">HONOURABLE</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0050">THE</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-0060">HOUSE</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0070">OF</w>
     <w lemma="peer" pos="n2" xml:id="A94613-001-a-0080">PEERS</w>
     <w lemma="now" pos="av" xml:id="A94613-001-a-0090">NOW</w>
     <w lemma="assemble" pos="vvn" xml:id="A94613-001-a-0100">ASSEMBLED</w>
     <w lemma="in" pos="acp" xml:id="A94613-001-a-0110">IN</w>
     <w lemma="parliament" pos="n1" xml:id="A94613-001-a-0120">PARLIAMENT</w>
     <pc unit="sentence" xml:id="A94613-001-a-0130">.</pc>
    </head>
    <head type="sub" xml:id="A94613-e50">
     <w lemma="the" pos="d" xml:id="A94613-001-a-0140">The</w>
     <w lemma="humble" pos="j" xml:id="A94613-001-a-0150">humble</w>
     <w lemma="petition" pos="n1" xml:id="A94613-001-a-0160">Petition</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0170">of</w>
     <w lemma="knight" pos="n2" xml:id="A94613-001-a-0180">Knights</w>
     <pc xml:id="A94613-001-a-0190">,</pc>
     <w lemma="gentleman" pos="n2" xml:id="A94613-001-a-0200">Gentlemen</w>
     <pc xml:id="A94613-001-a-0210">,</pc>
     <w lemma="freeholder" pos="n2" xml:id="A94613-001-a-0220">Freeholders</w>
     <pc xml:id="A94613-001-a-0230">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-0240">and</w>
     <w lemma="other" pos="pi2-d" xml:id="A94613-001-a-0250">others</w>
     <w lemma="inhabitant" pos="n2" xml:id="A94613-001-a-0260">Inhabitants</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0270">of</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0280">the</w>
     <w lemma="county" pos="n1" xml:id="A94613-001-a-0290">County</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0300">of</w>
     <w lemma="KENT" pos="nn1" xml:id="A94613-001-a-0310">KENT</w>
     <pc unit="sentence" xml:id="A94613-001-a-0320">.</pc>
    </head>
    <opener xml:id="A94613-e60">
     <salute xml:id="A94613-e70">
      <w lemma="show" pos="vvz" reg="Showeth" xml:id="A94613-001-a-0330">Sheweth</w>
      <pc xml:id="A94613-001-a-0340">,</pc>
     </salute>
    </opener>
    <p xml:id="A94613-e80">
     <w lemma="that" pos="cs" rend="decorinit" xml:id="A94613-001-a-0350">THat</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0360">the</w>
     <w lemma="petitioner" pos="n2" xml:id="A94613-001-a-0370">Petitioners</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A94613-001-a-0380">doe</w>
     <w lemma="with" pos="acp" xml:id="A94613-001-a-0390">with</w>
     <w lemma="joy" pos="n1" xml:id="A94613-001-a-0400">joy</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-0410">and</w>
     <w lemma="humble" pos="j" xml:id="A94613-001-a-0420">humble</w>
     <w lemma="thankfulness" pos="n1" reg="thankfulness" xml:id="A94613-001-a-0430">thankfullnesse</w>
     <pc xml:id="A94613-001-a-0440">,</pc>
     <w lemma="acknowledge" pos="vvb" xml:id="A94613-001-a-0450">acknowledge</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0460">the</w>
     <w lemma="good" pos="j" xml:id="A94613-001-a-0470">good</w>
     <w lemma="correspondence" pos="n1" xml:id="A94613-001-a-0480">correspondence</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-0490">and</w>
     <w lemma="concurrence" pos="n1" xml:id="A94613-001-a-0500">concurrence</w>
     <pc xml:id="A94613-001-a-0510">,</pc>
     <w lemma="which" pos="crq" xml:id="A94613-001-a-0520">which</w>
     <pc join="right" xml:id="A94613-001-a-0530">(</pc>
     <w lemma="by" pos="acp" xml:id="A94613-001-a-0540">by</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0550">the</w>
     <w lemma="blessing" pos="n1" xml:id="A94613-001-a-0560">blessing</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0570">of</w>
     <w lemma="God" pos="nn1" xml:id="A94613-001-a-0580">God</w>
     <pc xml:id="A94613-001-a-0590">)</pc>
     <w lemma="this" pos="d" xml:id="A94613-001-a-0600">this</w>
     <w lemma="honourable" pos="j" xml:id="A94613-001-a-0610">Honourable</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-0620">House</w>
     <w lemma="have" pos="vvz" xml:id="A94613-001-a-0630">hath</w>
     <w lemma="hold" pos="vvn" xml:id="A94613-001-a-0640">held</w>
     <w lemma="with" pos="acp" xml:id="A94613-001-a-0650">with</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0660">the</w>
     <w lemma="worthy" pos="j" xml:id="A94613-001-a-0670">worthy</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-0680">House</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0690">of</w>
     <w lemma="commons" pos="n2" xml:id="A94613-001-a-0700">Commons</w>
     <pc xml:id="A94613-001-a-0710">,</pc>
     <w lemma="in" pos="acp" xml:id="A94613-001-a-0720">in</w>
     <w lemma="pass" pos="vvg" xml:id="A94613-001-a-0730">passing</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0740">the</w>
     <w lemma="bill" pos="n1" xml:id="A94613-001-a-0750">Bill</w>
     <pc xml:id="A94613-001-a-0760">,</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-0770">to</w>
     <w lemma="take" pos="vvi" xml:id="A94613-001-a-0780">take</w>
     <w lemma="away" pos="av" xml:id="A94613-001-a-0790">away</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0800">the</w>
     <w lemma="vote" pos="n2" xml:id="A94613-001-a-0810">Votes</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-0820">of</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-0830">the</w>
     <w lemma="prelate" pos="n2" xml:id="A94613-001-a-0840">Prelates</w>
     <w lemma="in" pos="acp" xml:id="A94613-001-a-0850">in</w>
     <w lemma="this" pos="d" xml:id="A94613-001-a-0860">this</w>
     <w lemma="honourable" pos="j" xml:id="A94613-001-a-0870">Honourable</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-0880">House</w>
     <pc xml:id="A94613-001-a-0890">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-0900">and</w>
     <w lemma="disable" pos="n1-vg" xml:id="A94613-001-a-0910">disabling</w>
     <w lemma="they" pos="pno" xml:id="A94613-001-a-0920">them</w>
     <w lemma="from" pos="acp" xml:id="A94613-001-a-0930">from</w>
     <w lemma="temporal" pos="j" reg="temporal" xml:id="A94613-001-a-0940">Temporall</w>
     <w lemma="employment" pos="n2" reg="employments" xml:id="A94613-001-a-0950">imployments</w>
     <pc xml:id="A94613-001-a-0960">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-0970">and</w>
     <w lemma="for" pos="acp" xml:id="A94613-001-a-0980">for</w>
     <w lemma="set" pos="vvg" xml:id="A94613-001-a-0990">setting</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1000">the</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A94613-001-a-1010">Kingdome</w>
     <w lemma="into" pos="acp" xml:id="A94613-001-a-1020">into</w>
     <w lemma="a" pos="d" xml:id="A94613-001-a-1030">a</w>
     <w lemma="posture" pos="n1" xml:id="A94613-001-a-1040">Posture</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-1050">of</w>
     <w lemma="war" pos="n1" reg="war" xml:id="A94613-001-a-1060">Warre</w>
     <w lemma="for" pos="acp" xml:id="A94613-001-a-1070">for</w>
     <w lemma="its" pos="po" xml:id="A94613-001-a-1080">its</w>
     <w lemma="defence" pos="n1" xml:id="A94613-001-a-1090">Defence</w>
     <pc unit="sentence" xml:id="A94613-001-a-1100">.</pc>
    </p>
    <p xml:id="A94613-e90">
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1110">And</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1120">the</w>
     <w lemma="petitioner" pos="n2" xml:id="A94613-001-a-1130">Petitioners</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A94613-001-a-1140">doe</w>
     <w lemma="in" pos="acp" xml:id="A94613-001-a-1150">in</w>
     <w lemma="like" pos="acp" xml:id="A94613-001-a-1160">like</w>
     <w lemma="manner" pos="n1" xml:id="A94613-001-a-1170">manner</w>
     <w lemma="most" pos="avs-d" xml:id="A94613-001-a-1180">most</w>
     <w lemma="humble" pos="av-j" xml:id="A94613-001-a-1190">humbly</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1200">and</w>
     <w lemma="hearty" pos="av-j" xml:id="A94613-001-a-1210">heartily</w>
     <w lemma="profess" pos="vvi" reg="profess" xml:id="A94613-001-a-1220">professe</w>
     <pc xml:id="A94613-001-a-1230">,</pc>
     <w lemma="that" pos="cs" xml:id="A94613-001-a-1240">that</w>
     <w lemma="they" pos="pns" xml:id="A94613-001-a-1250">they</w>
     <w lemma="will" pos="vmb" xml:id="A94613-001-a-1260">will</w>
     <w lemma="ever" pos="av" xml:id="A94613-001-a-1270">ever</w>
     <w lemma="honour" pos="vvi" xml:id="A94613-001-a-1280">honour</w>
     <w lemma="this" pos="d" xml:id="A94613-001-a-1290">this</w>
     <w lemma="honourable" pos="j" xml:id="A94613-001-a-1300">Honourable</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-1310">House</w>
     <pc xml:id="A94613-001-a-1320">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1330">and</w>
     <w lemma="to" pos="acp" xml:id="A94613-001-a-1340">to</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1350">the</w>
     <w lemma="utmost" pos="j" xml:id="A94613-001-a-1360">utmost</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-1370">of</w>
     <w lemma="their" pos="po" xml:id="A94613-001-a-1380">their</w>
     <w lemma="power" pos="n1" xml:id="A94613-001-a-1390">power</w>
     <w lemma="defend" pos="vvi" xml:id="A94613-001-a-1400">defend</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1410">the</w>
     <w lemma="same" pos="d" xml:id="A94613-001-a-1420">same</w>
     <pc xml:id="A94613-001-a-1430">,</pc>
     <w lemma="so" pos="av" xml:id="A94613-001-a-1440">so</w>
     <w lemma="far" pos="av-j" reg="far" xml:id="A94613-001-a-1450">farre</w>
     <w lemma="as" pos="acp" xml:id="A94613-001-a-1460">as</w>
     <w lemma="their" pos="po" xml:id="A94613-001-a-1470">their</w>
     <w lemma="lordship" pos="n2" xml:id="A94613-001-a-1480">Lordships</w>
     <w lemma="shall" pos="vmb" xml:id="A94613-001-a-1490">shall</w>
     <w lemma="continue" pos="vvi" xml:id="A94613-001-a-1500">continue</w>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-1510">to</w>
     <w lemma="hold" pos="vvi" xml:id="A94613-001-a-1520">hold</w>
     <w lemma="correspondence" pos="n1" xml:id="A94613-001-a-1530">correspondence</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1540">and</w>
     <w lemma="concurrence" pos="n1" xml:id="A94613-001-a-1550">concurrence</w>
     <w lemma="with" pos="acp" xml:id="A94613-001-a-1560">with</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1570">the</w>
     <w lemma="say" pos="j-vn" xml:id="A94613-001-a-1580">said</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-1590">House</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-1600">of</w>
     <w lemma="commons" pos="n2" xml:id="A94613-001-a-1610">Commons</w>
     <pc xml:id="A94613-001-a-1620">,</pc>
     <w lemma="in" pos="acp" xml:id="A94613-001-a-1630">in</w>
     <w lemma="all" pos="d" xml:id="A94613-001-a-1640">all</w>
     <w lemma="their" pos="po" xml:id="A94613-001-a-1650">their</w>
     <w lemma="just" pos="j" xml:id="A94613-001-a-1660">just</w>
     <w lemma="desire" pos="n2" xml:id="A94613-001-a-1670">desires</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1680">and</w>
     <w lemma="endeavour" pos="n2" xml:id="A94613-001-a-1690">endeavours</w>
     <pc xml:id="A94613-001-a-1700">,</pc>
     <w lemma="upon" pos="acp" xml:id="A94613-001-a-1710">upon</w>
     <w lemma="which" pos="crq" xml:id="A94613-001-a-1720">which</w>
     <pc xml:id="A94613-001-a-1730">,</pc>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1740">the</w>
     <w lemma="petitioner" pos="n2" xml:id="A94613-001-a-1750">Petitioners</w>
     <w lemma="do" pos="vvb" reg="do" xml:id="A94613-001-a-1760">doe</w>
     <w lemma="humble" pos="av-j" xml:id="A94613-001-a-1770">humbly</w>
     <w lemma="conceive" pos="vvi" xml:id="A94613-001-a-1780">conceive</w>
     <pc xml:id="A94613-001-a-1790">,</pc>
     <w lemma="great" pos="av-j" xml:id="A94613-001-a-1800">greatly</w>
     <w lemma="depend" pos="vvz" xml:id="A94613-001-a-1810">dependeth</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1820">the</w>
     <w lemma="peace" pos="n1" xml:id="A94613-001-a-1830">peace</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1840">and</w>
     <w lemma="welfare" pos="n1" xml:id="A94613-001-a-1850">welfare</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-1860">of</w>
     <w lemma="this" pos="d" xml:id="A94613-001-a-1870">this</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="A94613-001-a-1880">Kingdome</w>
     <pc unit="sentence" xml:id="A94613-001-a-1890">.</pc>
    </p>
    <p xml:id="A94613-e100">
     <w lemma="and" pos="cc" xml:id="A94613-001-a-1900">And</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-1910">the</w>
     <w lemma="petitioner" pos="n2" xml:id="A94613-001-a-1920">Petitioners</w>
     <w lemma="most" pos="avs-d" xml:id="A94613-001-a-1930">most</w>
     <w lemma="humble" pos="av-j" xml:id="A94613-001-a-1940">humbly</w>
     <w lemma="pray" pos="vvb" xml:id="A94613-001-a-1950">pray</w>
     <pc xml:id="A94613-001-a-1960">,</pc>
     <w lemma="that" pos="cs" xml:id="A94613-001-a-1970">that</w>
     <w lemma="this" pos="d" xml:id="A94613-001-a-1980">this</w>
     <w lemma="honourable" pos="j" xml:id="A94613-001-a-1990">Honourable</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-2000">House</w>
     <pc xml:id="A94613-001-a-2010">,</pc>
     <pc join="right" xml:id="A94613-001-a-2020">(</pc>
     <w lemma="declare" pos="vvg" xml:id="A94613-001-a-2030">declaring</w>
     <w lemma="therein" pos="av" reg="therein" xml:id="A94613-001-a-2040">therin</w>
     <w lemma="your" pos="po" xml:id="A94613-001-a-2050">your</w>
     <w lemma="noble" pos="j" xml:id="A94613-001-a-2060">Noble</w>
     <w lemma="resolution" pos="n1" xml:id="A94613-001-a-2070">Resolution</w>
     <w lemma="for" pos="acp" xml:id="A94613-001-a-2080">for</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-2090">the</w>
     <w lemma="public" pos="j" reg="public" xml:id="A94613-001-a-2100">publike</w>
     <w lemma="good" pos="j" xml:id="A94613-001-a-2110">good</w>
     <pc xml:id="A94613-001-a-2120">)</pc>
     <w lemma="will" pos="vmd" xml:id="A94613-001-a-2130">would</w>
     <w lemma="be" pos="vvi" xml:id="A94613-001-a-2140">be</w>
     <w lemma="please" pos="vvn" xml:id="A94613-001-a-2150">pleased</w>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2160">to</w>
     <w lemma="go" pos="vvi" reg="go" xml:id="A94613-001-a-2170">goe</w>
     <w lemma="on" pos="acp" xml:id="A94613-001-a-2180">on</w>
     <w lemma="with" pos="acp" xml:id="A94613-001-a-2190">with</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-2200">the</w>
     <w lemma="say" pos="j-vn" xml:id="A94613-001-a-2210">said</w>
     <w lemma="house" pos="n1" xml:id="A94613-001-a-2220">House</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-2230">of</w>
     <w lemma="commons" pos="n2" xml:id="A94613-001-a-2240">Commons</w>
     <w lemma="to" pos="acp" xml:id="A94613-001-a-2250">to</w>
     <w lemma="a" pos="d" xml:id="A94613-001-a-2260">a</w>
     <w lemma="through" pos="acp" xml:id="A94613-001-a-2270">through</w>
     <w lemma="reformation" pos="n1" xml:id="A94613-001-a-2280">Reformation</w>
     <pc xml:id="A94613-001-a-2290">,</pc>
     <w lemma="especial" pos="av-j" xml:id="A94613-001-a-2300">especially</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-2310">of</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-2320">the</w>
     <w lemma="church" pos="n1" xml:id="A94613-001-a-2330">Church</w>
     <pc xml:id="A94613-001-a-2340">,</pc>
     <w lemma="according" pos="j" xml:id="A94613-001-a-2350">according</w>
     <w lemma="to" pos="acp" xml:id="A94613-001-a-2360">to</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-2370">the</w>
     <w lemma="word" pos="n1" xml:id="A94613-001-a-2380">Word</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-2390">of</w>
     <w lemma="God" pos="nn1" xml:id="A94613-001-a-2400">God</w>
     <pc xml:id="A94613-001-a-2410">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2420">to</w>
     <w lemma="press" pos="vvi" reg="press" xml:id="A94613-001-a-2430">presse</w>
     <w lemma="dispatch" pos="n1" xml:id="A94613-001-a-2440">dispatch</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-2450">of</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-2460">the</w>
     <w lemma="aid" pos="n1" reg="aid" xml:id="A94613-001-a-2470">Ayd</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-2480">of</w>
     <hi xml:id="A94613-e110">
      <w lemma="Ireland" pos="nn1" xml:id="A94613-001-a-2490">Ireland</w>
      <pc xml:id="A94613-001-a-2500">,</pc>
     </hi>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2510">to</w>
     <w lemma="expedite" pos="vvi" xml:id="A94613-001-a-2520">expedite</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A94613-001-a-2530">proceedings</w>
     <w lemma="against" pos="acp" xml:id="A94613-001-a-2540">against</w>
     <w lemma="delinquent" pos="n2-j" xml:id="A94613-001-a-2550">delinquents</w>
     <pc xml:id="A94613-001-a-2560">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2570">to</w>
     <w lemma="vindicate" pos="vvi" xml:id="A94613-001-a-2580">vindicate</w>
     <w lemma="parliament" pos="n1" xml:id="A94613-001-a-2590">Parliament</w>
     <w lemma="privilege" pos="n2" reg="privileges" xml:id="A94613-001-a-2600">Priviledges</w>
     <pc xml:id="A94613-001-a-2610">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2620">to</w>
     <w lemma="discover" pos="vvi" xml:id="A94613-001-a-2630">discover</w>
     <pc xml:id="A94613-001-a-2640">,</pc>
     <w lemma="remove" pos="vvb" xml:id="A94613-001-a-2650">remove</w>
     <pc xml:id="A94613-001-a-2660">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-2670">and</w>
     <w lemma="punish" pos="vvi" xml:id="A94613-001-a-2680">punish</w>
     <w lemma="evil" pos="j" reg="evil" xml:id="A94613-001-a-2690">evill</w>
     <w lemma="counsel" pos="n2" reg="counsels" xml:id="A94613-001-a-2700">Councells</w>
     <pc xml:id="A94613-001-a-2710">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2720">to</w>
     <w lemma="deprive" pos="vvi" xml:id="A94613-001-a-2730">deprive</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-2740">the</w>
     <w lemma="popish" pos="jnn" xml:id="A94613-001-a-2750">Popish</w>
     <w lemma="lord" pos="n2" xml:id="A94613-001-a-2760">Lords</w>
     <w lemma="of" pos="acp" xml:id="A94613-001-a-2770">of</w>
     <w lemma="their" pos="po" xml:id="A94613-001-a-2780">their</w>
     <w lemma="vote" pos="n2" xml:id="A94613-001-a-2790">Votes</w>
     <pc xml:id="A94613-001-a-2800">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2810">to</w>
     <w lemma="disarm" pos="vvi" reg="disarm" xml:id="A94613-001-a-2820">disarme</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-2830">and</w>
     <w lemma="search" pos="vvi" xml:id="A94613-001-a-2840">search</w>
     <w lemma="out" pos="av" xml:id="A94613-001-a-2850">out</w>
     <w lemma="papist" pos="nn2" xml:id="A94613-001-a-2860">Papists</w>
     <pc xml:id="A94613-001-a-2870">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-2880">and</w>
     <w lemma="put" pos="vvd" xml:id="A94613-001-a-2890">put</w>
     <w lemma="they" pos="pno" xml:id="A94613-001-a-2900">them</w>
     <w lemma="into" pos="acp" xml:id="A94613-001-a-2910">into</w>
     <w lemma="safe" pos="j" xml:id="A94613-001-a-2920">safe</w>
     <w lemma="custody" pos="n1" xml:id="A94613-001-a-2930">custody</w>
     <pc xml:id="A94613-001-a-2940">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-2950">to</w>
     <w lemma="suppress" pos="vvi" reg="suppress" xml:id="A94613-001-a-2960">suppresse</w>
     <w lemma="mass" pos="n1" reg="mass" xml:id="A94613-001-a-2970">Masse</w>
     <pc xml:id="A94613-001-a-2980">,</pc>
     <w lemma="both" pos="av-d" xml:id="A94613-001-a-2990">both</w>
     <w lemma="in" pos="acp" xml:id="A94613-001-a-3000">in</w>
     <w lemma="public" pos="j" reg="public" xml:id="A94613-001-a-3010">publike</w>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-3020">and</w>
     <w lemma="private" pos="j" xml:id="A94613-001-a-3030">private</w>
     <pc xml:id="A94613-001-a-3040">;</pc>
     <w lemma="to" pos="prt" xml:id="A94613-001-a-3050">to</w>
     <w lemma="cast" pos="vvi" xml:id="A94613-001-a-3060">cast</w>
     <w lemma="out" pos="av" xml:id="A94613-001-a-3070">out</w>
     <w lemma="scandalous" pos="j" xml:id="A94613-001-a-3080">scandalous</w>
     <w lemma="minister" pos="n2" xml:id="A94613-001-a-3090">Ministers</w>
     <pc xml:id="A94613-001-a-3100">;</pc>
     <w lemma="plant" pos="vvb" xml:id="A94613-001-a-3110">plant</w>
     <w lemma="painful" pos="j" reg="painful" xml:id="A94613-001-a-3120">painfull</w>
     <w lemma="preacher" pos="n2" xml:id="A94613-001-a-3130">Preachers</w>
     <w lemma="everywhere" pos="av" reg="everywhere" xml:id="A94613-001-a-3140">every where</w>
     <pc xml:id="A94613-001-a-3160">,</pc>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-3170">and</w>
     <w lemma="discover" pos="vvi" xml:id="A94613-001-a-3180">discover</w>
     <w lemma="who" pos="crq" xml:id="A94613-001-a-3190">who</w>
     <w lemma="be" pos="vvb" xml:id="A94613-001-a-3200">are</w>
     <w lemma="church" pos="n1" xml:id="A94613-001-a-3210">Church</w>
     <w lemma="papist" pos="nn2" xml:id="A94613-001-a-3220">Papists</w>
     <pc xml:id="A94613-001-a-3230">,</pc>
     <w lemma="aswell" pos="av" reg="as well" xml:id="A94613-001-a-3240">aswell</w>
     <w lemma="as" pos="acp" xml:id="A94613-001-a-3250">as</w>
     <w lemma="know" pos="vvn" xml:id="A94613-001-a-3260">known</w>
     <w lemma="recusant" pos="n2" xml:id="A94613-001-a-3270">Recusants</w>
     <pc unit="sentence" xml:id="A94613-001-a-3280">.</pc>
    </p>
    <closer xml:id="A94613-e120">
     <w lemma="and" pos="cc" xml:id="A94613-001-a-3290">And</w>
     <w lemma="the" pos="d" xml:id="A94613-001-a-3300">the</w>
     <w lemma="petitioner" pos="n2" xml:id="A94613-001-a-3310">Petitioners</w>
     <w lemma="shall" pos="vmb" xml:id="A94613-001-a-3320">shall</w>
     <w lemma="daily" pos="av-j" reg="daily" xml:id="A94613-001-a-3330">dayly</w>
     <w lemma="pray" pos="vvi" xml:id="A94613-001-a-3340">pray</w>
     <pc xml:id="A94613-001-a-3350">,</pc>
     <w lemma="etc." pos="ab" xml:id="A94613-001-a-3360">&amp;c.</w>
     <pc unit="sentence" xml:id="A94613-001-a-3370"/>
    </closer>
   </div>
  </body>
  <back xml:id="A94613-e130">
   <div type="colophon" xml:id="A94613-e140">
    <p xml:id="A94613-e150">
     <hi xml:id="A94613-e160">
      <w lemma="london" pos="nn1" xml:id="A94613-001-a-3380">London</w>
      <pc xml:id="A94613-001-a-3390">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A94613-001-a-3400">Printed</w>
     <w lemma="by" pos="acp" xml:id="A94613-001-a-3410">by</w>
     <hi xml:id="A94613-e170">
      <w lemma="r." pos="ab" xml:id="A94613-001-a-3420">R.</w>
      <w lemma="Oulton" pos="nn1" xml:id="A94613-001-a-3430">Oulton</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A94613-001-a-3440">and</w>
     <hi xml:id="A94613-e180">
      <w lemma="g." pos="ab" xml:id="A94613-001-a-3450">G.</w>
      <w lemma="dexter" pos="nn1" xml:id="A94613-001-a-3460">Dexter</w>
      <pc unit="sentence" xml:id="A94613-001-a-3470">.</pc>
     </hi>
     <w lemma="1641." pos="crd" xml:id="A94613-001-a-3480">1641.</w>
     <pc unit="sentence" xml:id="A94613-001-a-3490"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
