<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A94624">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the Right Honourable the knights, citizens, and burgesses, assembled in Parliament the humble petition of the workmen-printers, freemen of the city of London[.]</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A94624 of text R211152 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.21[19]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A94624</idno>
    <idno type="STC">Wing T1648</idno>
    <idno type="STC">Thomason 669.f.21[19]</idno>
    <idno type="STC">ESTC R211152</idno>
    <idno type="EEBO-CITATION">99869885</idno>
    <idno type="PROQUEST">99869885</idno>
    <idno type="VID">163514</idno>
    <idno type="PROQUESTGOID">2240954303</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A94624)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163514)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 247:669f21[19])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the Right Honourable the knights, citizens, and burgesses, assembled in Parliament the humble petition of the workmen-printers, freemen of the city of London[.]</title>
      <author>Hawkins, George, fl. 1659.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1659]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed: George Hawkins (and 9 others).</note>
      <note>Imprint from Wing.</note>
      <note>Protesting against the monopoly for printing Bibles, at present possessed by Henry Hills and John Field.</note>
      <note>Annotation on Thomason copy: "April. 14. 1659".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Bible -- Publication and distribution -- England -- Early works to 1800.</term>
     <term>Printing -- Great Britain -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A94624</ep:tcp>
    <ep:estc> R211152</ep:estc>
    <ep:stc> (Thomason 669.f.21[19]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>To the Right Honourable the knights, citizens, and burgesses, assembled in Parliament, the humble petition of the workmen-printers, freemen</ep:title>
    <ep:author>Hawkins, George</ep:author>
    <ep:publicationYear>1659</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>349</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-06</date><label>Aptara</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-07</date><label>Angela Berkley</label>
        Sampled and proofread
      </change>
   <change><date>2007-07</date><label>Angela Berkley</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A94624-e10">
  <body xml:id="A94624-e20">
   <pb facs="tcp:163514:1" rend="simple:additions" xml:id="A94624-001-a"/>
   <div type="petition" xml:id="A94624-e30">
    <head xml:id="A94624-e40">
     <w lemma="to" pos="acp" xml:id="A94624-001-a-0010">To</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0020">the</w>
     <w lemma="right" pos="n1-j" xml:id="A94624-001-a-0030">Right</w>
     <w lemma="honourable" pos="j" xml:id="A94624-001-a-0040">Honourable</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0050">The</w>
     <w lemma="knight" pos="n2" xml:id="A94624-001-a-0060">Knights</w>
     <pc xml:id="A94624-001-a-0070">,</pc>
     <w lemma="citizen" pos="n2" xml:id="A94624-001-a-0080">Citizens</w>
     <pc xml:id="A94624-001-a-0090">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-0100">and</w>
     <w lemma="burgess" pos="n2" xml:id="A94624-001-a-0110">Burgesses</w>
     <pc xml:id="A94624-001-a-0120">,</pc>
     <w lemma="assemble" pos="vvn" xml:id="A94624-001-a-0130">assembled</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-0140">IN</w>
     <w lemma="parliament" pos="n1" xml:id="A94624-001-a-0150">PARLIAMENT</w>
     <pc xml:id="A94624-001-a-0160">,</pc>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0170">The</w>
     <w lemma="humble" pos="j" xml:id="A94624-001-a-0180">humble</w>
     <w lemma="petition" pos="n1" xml:id="A94624-001-a-0190">Petition</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0200">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0210">the</w>
     <w lemma="workmen-printer" pos="n2" xml:id="A94624-001-a-0220">Workmen-Printers</w>
     <pc xml:id="A94624-001-a-0230">,</pc>
     <w lemma="freeman" pos="n2" xml:id="A94624-001-a-0240">Freemen</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0250">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0260">the</w>
     <w lemma="city" pos="n1" xml:id="A94624-001-a-0270">City</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0280">of</w>
     <w lemma="London" pos="nn1" xml:id="A94624-001-a-0290">London</w>
     <pc xml:id="A94624-001-a-0300">,</pc>
    </head>
    <head type="sub" xml:id="A94624-e50">
     <w lemma="show" pos="vvz" reg="Showeth" xml:id="A94624-001-a-0310">Sheweth</w>
     <pc xml:id="A94624-001-a-0320">,</pc>
    </head>
    <p xml:id="A94624-e60">
     <w lemma="that" pos="cs" xml:id="A94624-001-a-0330">THat</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-0340">in</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0350">the</w>
     <w lemma="time" pos="n1" xml:id="A94624-001-a-0360">time</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0370">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0380">the</w>
     <w lemma="late" pos="j" xml:id="A94624-001-a-0390">late</w>
     <w lemma="general" pos="j" reg="general" xml:id="A94624-001-a-0400">generall</w>
     <w lemma="liberty" pos="n1" xml:id="A94624-001-a-0410">Liberty</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0420">of</w>
     <w lemma="print" pos="n1-vg" xml:id="A94624-001-a-0430">Printing</w>
     <w lemma="English" pos="jnn" xml:id="A94624-001-a-0440">English</w>
     <w lemma="Bible" pos="n2" xml:id="A94624-001-a-0450">Bibles</w>
     <pc xml:id="A94624-001-a-0460">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-0470">and</w>
     <w lemma="testament" pos="n2" xml:id="A94624-001-a-0480">Testaments</w>
     <w lemma="at" pos="acp" xml:id="A94624-001-a-0490">at</w>
     <hi xml:id="A94624-e70">
      <w lemma="London" pos="nn1" xml:id="A94624-001-a-0500">London</w>
      <pc xml:id="A94624-001-a-0510">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-0520">and</w>
     <w lemma="until" pos="acp" reg="until" xml:id="A94624-001-a-0530">untill</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0540">the</w>
     <w lemma="6." pos="crd" xml:id="A94624-001-a-0550">6.</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0560">of</w>
     <hi xml:id="A94624-e80">
      <w lemma="march" pos="nn1" xml:id="A94624-001-a-0570">March</w>
     </hi>
     <w lemma="1655." pos="crd" xml:id="A94624-001-a-0580">1655.</w>
     <w lemma="last" pos="ord" xml:id="A94624-001-a-0590">last</w>
     <w lemma="past" pos="j" xml:id="A94624-001-a-0600">past</w>
     <pc xml:id="A94624-001-a-0610">,</pc>
     <w lemma="there" pos="av" xml:id="A94624-001-a-0620">there</w>
     <w lemma="be" pos="vvd" xml:id="A94624-001-a-0630">were</w>
     <w lemma="above" pos="acp" xml:id="A94624-001-a-0640">above</w>
     <w lemma="sixty" pos="crd" xml:id="A94624-001-a-0650">sixty</w>
     <w lemma="workmen-printer" pos="n2" xml:id="A94624-001-a-0660">Workmen-Printers</w>
     <w lemma="employ" pos="vvn" reg="employed" xml:id="A94624-001-a-0670">imployed</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-0680">in</w>
     <w lemma="constant" pos="j" xml:id="A94624-001-a-0690">constant</w>
     <w lemma="work" pos="n1" xml:id="A94624-001-a-0700">work</w>
     <w lemma="at" pos="acp" xml:id="A94624-001-a-0710">at</w>
     <w lemma="several" pos="j" reg="several" xml:id="A94624-001-a-0720">severall</w>
     <w lemma="printing-house" pos="n2" xml:id="A94624-001-a-0730">Printing-houses</w>
     <pc xml:id="A94624-001-a-0740">;</pc>
     <w lemma="whereby" pos="crq" xml:id="A94624-001-a-0750">whereby</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0760">the</w>
     <w lemma="book" pos="n2" xml:id="A94624-001-a-0770">Books</w>
     <w lemma="be" pos="vvd" xml:id="A94624-001-a-0780">were</w>
     <w lemma="vend" pos="vvn" xml:id="A94624-001-a-0790">vended</w>
     <w lemma="at" pos="acp" xml:id="A94624-001-a-0800">at</w>
     <w lemma="very" pos="av" xml:id="A94624-001-a-0810">very</w>
     <w lemma="moderate" pos="j" xml:id="A94624-001-a-0820">moderate</w>
     <w lemma="rate" pos="n2" xml:id="A94624-001-a-0830">rates</w>
     <pc xml:id="A94624-001-a-0840">,</pc>
     <w lemma="to" pos="acp" xml:id="A94624-001-a-0850">to</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0860">the</w>
     <w lemma="great" pos="j" xml:id="A94624-001-a-0870">great</w>
     <w lemma="accommodation" pos="n1" xml:id="A94624-001-a-0880">accommodation</w>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-0890">and</w>
     <w lemma="benefit" pos="n1" xml:id="A94624-001-a-0900">benefit</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-0910">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-0920">the</w>
     <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A94624-001-a-0930">Common-wealth</w>
     <pc xml:id="A94624-001-a-0940">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-0950">and</w>
     <w lemma="encouragement" pos="n1" xml:id="A94624-001-a-0960">encouragement</w>
     <pc xml:id="A94624-001-a-0970">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-0980">and</w>
     <w lemma="comfortable" pos="j" xml:id="A94624-001-a-0990">comfortable</w>
     <w lemma="support" pos="n1" xml:id="A94624-001-a-1000">support</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1010">of</w>
     <w lemma="your" pos="po" xml:id="A94624-001-a-1020">your</w>
     <w lemma="petitioner" pos="n2" xml:id="A94624-001-a-1030">Petitioners</w>
     <pc xml:id="A94624-001-a-1040">;</pc>
     <w lemma="but" pos="acp" xml:id="A94624-001-a-1050">but</w>
     <w lemma="by" pos="acp" xml:id="A94624-001-a-1060">by</w>
     <w lemma="mean" pos="n2" xml:id="A94624-001-a-1070">means</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1080">of</w>
     <w lemma="a" pos="d" xml:id="A94624-001-a-1090">an</w>
     <w lemma="undue" pos="j" xml:id="A94624-001-a-1100">undue</w>
     <pc xml:id="A94624-001-a-1110">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-1120">and</w>
     <w lemma="unlawful" pos="j" reg="unlawful" xml:id="A94624-001-a-1130">unlawfull</w>
     <w lemma="entrance" pos="n1" xml:id="A94624-001-a-1140">entrance</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1150">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-1160">the</w>
     <w lemma="late" pos="j" xml:id="A94624-001-a-1170">late</w>
     <w lemma="translate" pos="j-vn" xml:id="A94624-001-a-1180">translated</w>
     <w lemma="copy" pos="n1" xml:id="A94624-001-a-1190">Copy</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1200">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-1210">the</w>
     <w lemma="bible" pos="n1" xml:id="A94624-001-a-1220">Bible</w>
     <pc xml:id="A94624-001-a-1230">,</pc>
     <w lemma="then" pos="av" xml:id="A94624-001-a-1240">then</w>
     <w lemma="make" pos="vvn" xml:id="A94624-001-a-1250">made</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-1260">in</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-1270">the</w>
     <w lemma="register" pos="n1" xml:id="A94624-001-a-1280">Register</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1290">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-1300">the</w>
     <w lemma="company" pos="n1" xml:id="A94624-001-a-1310">Company</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1320">of</w>
     <w lemma="stationer" pos="n2" xml:id="A94624-001-a-1330">Stationers</w>
     <pc xml:id="A94624-001-a-1340">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-1350">and</w>
     <w lemma="other" pos="d" xml:id="A94624-001-a-1360">other</w>
     <w lemma="unjust" pos="j" xml:id="A94624-001-a-1370">unjust</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A94624-001-a-1380">proceedings</w>
     <w lemma="use" pos="vvn" xml:id="A94624-001-a-1390">used</w>
     <w lemma="by" pos="acp" xml:id="A94624-001-a-1400">by</w>
     <hi xml:id="A94624-e90">
      <w lemma="Henry" pos="nn1" xml:id="A94624-001-a-1410">Henry</w>
      <w lemma="hill" pos="n2" xml:id="A94624-001-a-1420">Hills</w>
     </hi>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-1430">and</w>
     <hi xml:id="A94624-e100">
      <w lemma="John" pos="nn1" xml:id="A94624-001-a-1440">John</w>
      <w lemma="field" pos="nn1" xml:id="A94624-001-a-1450">Field</w>
     </hi>
     <w lemma="printer" pos="n2" xml:id="A94624-001-a-1460">Printers</w>
     <pc xml:id="A94624-001-a-1470">,</pc>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-1480">in</w>
     <w lemma="reference" pos="n1" xml:id="A94624-001-a-1490">reference</w>
     <w lemma="to" pos="acp" xml:id="A94624-001-a-1500">to</w>
     <w lemma="their" pos="po" xml:id="A94624-001-a-1510">their</w>
     <w lemma="monopolise" pos="vvg" reg="monopolising" xml:id="A94624-001-a-1520">Monopolizing</w>
     <w lemma="ever" pos="av" xml:id="A94624-001-a-1530">ever</w>
     <w lemma="since" pos="acp" xml:id="A94624-001-a-1540">since</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1550">of</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-1560">the</w>
     <w lemma="sole" pos="j" xml:id="A94624-001-a-1570">sole</w>
     <w lemma="print" pos="n1-vg" xml:id="A94624-001-a-1580">printing</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1590">of</w>
     <w lemma="Bible" pos="n2" xml:id="A94624-001-a-1600">Bibles</w>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-1610">and</w>
     <w lemma="testament" pos="n2" xml:id="A94624-001-a-1620">Testaments</w>
     <pc xml:id="A94624-001-a-1630">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-1640">and</w>
     <w lemma="suppress" pos="vvg" xml:id="A94624-001-a-1650">suppressing</w>
     <w lemma="all" pos="d" xml:id="A94624-001-a-1660">all</w>
     <w lemma="other" pos="d" xml:id="A94624-001-a-1670">other</w>
     <w lemma="master-printer" pos="n2" xml:id="A94624-001-a-1680">Master-Printers</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-1690">in</w>
     <w lemma="that" pos="d" xml:id="A94624-001-a-1700">that</w>
     <w lemma="good" pos="j" xml:id="A94624-001-a-1710">good</w>
     <w lemma="work" pos="n1" xml:id="A94624-001-a-1720">work</w>
     <pc xml:id="A94624-001-a-1730">;</pc>
     <pc join="right" xml:id="A94624-001-a-1740">(</pc>
     <w lemma="whereby" pos="crq" xml:id="A94624-001-a-1750">Whereby</w>
     <w lemma="they" pos="pns" xml:id="A94624-001-a-1760">they</w>
     <w lemma="themselves" pos="pr" xml:id="A94624-001-a-1770">themselves</w>
     <w lemma="employ" pos="vvb" reg="employ" xml:id="A94624-001-a-1780">imploy</w>
     <w lemma="not" pos="xx" xml:id="A94624-001-a-1790">not</w>
     <w lemma="above" pos="acp" xml:id="A94624-001-a-1800">above</w>
     <w lemma="ten" pos="crd" xml:id="A94624-001-a-1810">ten</w>
     <w lemma="or" pos="cc" xml:id="A94624-001-a-1820">or</w>
     <w lemma="twelve" pos="crd" xml:id="A94624-001-a-1830">twelve</w>
     <w lemma="person" pos="n2" xml:id="A94624-001-a-1840">persons</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-1850">in</w>
     <w lemma="their" pos="po" xml:id="A94624-001-a-1860">their</w>
     <w lemma="service" pos="n1" xml:id="A94624-001-a-1870">service</w>
     <pc xml:id="A94624-001-a-1880">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-1890">and</w>
     <w lemma="enhance" pos="vvi" reg="enhance" xml:id="A94624-001-a-1900">inhanse</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-1910">the</w>
     <w lemma="price" pos="n2" xml:id="A94624-001-a-1920">prices</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-1930">of</w>
     <w lemma="their" pos="po" xml:id="A94624-001-a-1940">their</w>
     <w lemma="book" pos="n2" xml:id="A94624-001-a-1950">Books</w>
     <w lemma="to" pos="acp" xml:id="A94624-001-a-1960">to</w>
     <w lemma="excessive" pos="j" xml:id="A94624-001-a-1970">excessive</w>
     <w lemma="dear" pos="j" xml:id="A94624-001-a-1980">dear</w>
     <w lemma="rate" pos="n2" xml:id="A94624-001-a-1990">rates</w>
     <pc xml:id="A94624-001-a-2000">,</pc>
     <pc xml:id="A94624-001-a-2010">)</pc>
     <w lemma="the" pos="d" xml:id="A94624-001-a-2020">The</w>
     <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A94624-001-a-2030">Common-wealth</w>
     <w lemma="be" pos="vvz" xml:id="A94624-001-a-2040">is</w>
     <w lemma="abuse" pos="vvn" xml:id="A94624-001-a-2050">abused</w>
     <w lemma="by" pos="acp" xml:id="A94624-001-a-2060">by</w>
     <w lemma="a" pos="d" xml:id="A94624-001-a-2070">an</w>
     <w lemma="unreasonable" pos="j" xml:id="A94624-001-a-2080">unreasonable</w>
     <w lemma="necessity" pos="n1" xml:id="A94624-001-a-2090">necessity</w>
     <pc xml:id="A94624-001-a-2100">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2110">and</w>
     <w lemma="your" pos="po" xml:id="A94624-001-a-2120">your</w>
     <w lemma="petitioner" pos="n2" xml:id="A94624-001-a-2130">Petitioners</w>
     <w lemma="be" pos="vvb" xml:id="A94624-001-a-2140">are</w>
     <w lemma="for" pos="acp" xml:id="A94624-001-a-2150">for</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-2160">the</w>
     <w lemma="generality" pos="n1" xml:id="A94624-001-a-2170">generality</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-2180">of</w>
     <w lemma="they" pos="pno" xml:id="A94624-001-a-2190">them</w>
     <w lemma="much" pos="av-d" xml:id="A94624-001-a-2200">much</w>
     <w lemma="prejudice" pos="vvn" xml:id="A94624-001-a-2210">prejudiced</w>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2220">and</w>
     <w lemma="abridge" pos="vvn" xml:id="A94624-001-a-2230">abridged</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-2240">in</w>
     <w lemma="their" pos="po" xml:id="A94624-001-a-2250">their</w>
     <w lemma="hope" pos="n2" xml:id="A94624-001-a-2260">hopes</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-2270">of</w>
     <w lemma="livelihood" pos="n1" reg="livelihood" xml:id="A94624-001-a-2280">lively-hood</w>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2290">and</w>
     <w lemma="fortune" pos="n2" xml:id="A94624-001-a-2300">fortunes</w>
     <pc xml:id="A94624-001-a-2310">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2320">and</w>
     <w lemma="many" pos="d" xml:id="A94624-001-a-2330">many</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-2340">of</w>
     <w lemma="they" pos="pno" xml:id="A94624-001-a-2350">them</w>
     <w lemma="reduce" pos="vvn" xml:id="A94624-001-a-2360">reduced</w>
     <w lemma="to" pos="acp" xml:id="A94624-001-a-2370">to</w>
     <w lemma="extreme" pos="j" reg="extreme" xml:id="A94624-001-a-2380">extream</w>
     <w lemma="want" pos="n1" xml:id="A94624-001-a-2390">want</w>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2400">and</w>
     <w lemma="poverty" pos="n1" xml:id="A94624-001-a-2410">poverty</w>
     <pc xml:id="A94624-001-a-2420">,</pc>
     <w lemma="to" pos="acp" xml:id="A94624-001-a-2430">to</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-2440">the</w>
     <w lemma="great" pos="j" xml:id="A94624-001-a-2450">great</w>
     <w lemma="grief" pos="n1" xml:id="A94624-001-a-2460">grief</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-2470">of</w>
     <w lemma="themselves" pos="pr" xml:id="A94624-001-a-2480">themselves</w>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2490">and</w>
     <w lemma="their" pos="po" xml:id="A94624-001-a-2500">their</w>
     <w lemma="relation" pos="n2" xml:id="A94624-001-a-2510">relations</w>
     <pc unit="sentence" xml:id="A94624-001-a-2520">.</pc>
    </p>
    <p xml:id="A94624-e110">
     <hi xml:id="A94624-e120">
      <w lemma="may" pos="vmb" xml:id="A94624-001-a-2530">May</w>
      <w lemma="it" pos="pn" xml:id="A94624-001-a-2540">it</w>
      <w lemma="therefore" pos="av" xml:id="A94624-001-a-2550">therefore</w>
      <w lemma="most" pos="avs-d" xml:id="A94624-001-a-2560">most</w>
      <w lemma="gracious" pos="av-j" xml:id="A94624-001-a-2570">graciously</w>
      <w lemma="please" pos="vvi" xml:id="A94624-001-a-2580">please</w>
      <w lemma="your" pos="po" xml:id="A94624-001-a-2590">your</w>
      <w lemma="honour" pos="n2" xml:id="A94624-001-a-2600">Honours</w>
      <w lemma="to" pos="prt" xml:id="A94624-001-a-2610">to</w>
      <w lemma="hear" pos="vvi" xml:id="A94624-001-a-2620">hear</w>
      <w lemma="and" pos="cc" xml:id="A94624-001-a-2630">and</w>
      <w lemma="redress" pos="vvi" reg="redress" xml:id="A94624-001-a-2640">redresse</w>
      <w lemma="your" pos="po" xml:id="A94624-001-a-2650">your</w>
      <w lemma="petitioner" pos="n2" xml:id="A94624-001-a-2660">Petitioners</w>
      <w lemma="grievance" pos="n2" xml:id="A94624-001-a-2670">Grievances</w>
      <w lemma="with" pos="acp" xml:id="A94624-001-a-2680">with</w>
      <w lemma="all" pos="d" xml:id="A94624-001-a-2690">all</w>
      <w lemma="convenience" pos="n1" xml:id="A94624-001-a-2700">convenience</w>
      <pc xml:id="A94624-001-a-2710">,</pc>
      <w lemma="be" pos="vvg" xml:id="A94624-001-a-2720">being</w>
      <w lemma="of" pos="acp" xml:id="A94624-001-a-2730">of</w>
      <w lemma="such" pos="d" xml:id="A94624-001-a-2740">such</w>
      <w lemma="universal" pos="j" reg="universal" xml:id="A94624-001-a-2750">universall</w>
      <w lemma="influence" pos="n1" xml:id="A94624-001-a-2760">influence</w>
      <w lemma="upon" pos="acp" xml:id="A94624-001-a-2770">upon</w>
      <pc xml:id="A94624-001-a-2780">,</pc>
      <w lemma="and" pos="cc" xml:id="A94624-001-a-2790">and</w>
      <w lemma="concernment" pos="n1" xml:id="A94624-001-a-2800">concernment</w>
      <w lemma="to" pos="acp" xml:id="A94624-001-a-2810">to</w>
      <w lemma="the" pos="d" xml:id="A94624-001-a-2820">the</w>
      <w lemma="good" pos="j" xml:id="A94624-001-a-2830">good</w>
      <w lemma="of" pos="acp" xml:id="A94624-001-a-2840">of</w>
      <w lemma="the" pos="d" xml:id="A94624-001-a-2850">the</w>
      <w lemma="commonwealth" pos="n1" reg="commonwealth" xml:id="A94624-001-a-2860">Common-wealth</w>
      <pc unit="sentence" xml:id="A94624-001-a-2870">.</pc>
     </hi>
    </p>
    <closer xml:id="A94624-e130">
     <w lemma="and" pos="cc" xml:id="A94624-001-a-2880">And</w>
     <w lemma="your" pos="po" xml:id="A94624-001-a-2890">your</w>
     <w lemma="petitioner" pos="n2" xml:id="A94624-001-a-2900">Petitioners</w>
     <pc xml:id="A94624-001-a-2910">,</pc>
     <w lemma="as" pos="acp" xml:id="A94624-001-a-2920">as</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-2930">in</w>
     <w lemma="duty" pos="n1" xml:id="A94624-001-a-2940">duty</w>
     <w lemma="bind" pos="vvn" xml:id="A94624-001-a-2950">bound</w>
     <pc xml:id="A94624-001-a-2960">,</pc>
     <w lemma="shall" pos="vmb" xml:id="A94624-001-a-2970">shall</w>
     <w lemma="ever" pos="av" xml:id="A94624-001-a-2980">ever</w>
     <w lemma="pray" pos="vvi" xml:id="A94624-001-a-2990">pray</w>
     <pc xml:id="A94624-001-a-3000">,</pc>
     <w lemma="etc." pos="ab" xml:id="A94624-001-a-3010">&amp;c.</w>
     <pc unit="sentence" xml:id="A94624-001-a-3020"/>
     <w lemma="sign" pos="j-vn" xml:id="A94624-001-a-3030">Signed</w>
     <w lemma="by" pos="acp" xml:id="A94624-001-a-3040">by</w>
     <signed xml:id="A94624-e140">
      <list xml:id="A94624-e150">
       <item xml:id="A94624-e160">
        <w lemma="George" pos="nn1" xml:id="A94624-001-a-3050">George</w>
        <w lemma="Hawkins" pos="nn1" xml:id="A94624-001-a-3060">Hawkins</w>
        <pc unit="sentence" xml:id="A94624-001-a-3070">.</pc>
       </item>
       <item xml:id="A94624-e170">
        <w lemma="John" pos="nn1" reg="John" xml:id="A94624-001-a-3080">Iohn</w>
        <w lemma="Sporier" pos="nn1" xml:id="A94624-001-a-3090">Sporier</w>
        <pc unit="sentence" xml:id="A94624-001-a-3100">.</pc>
       </item>
       <item xml:id="A94624-e180">
        <w lemma="Thomas" pos="nn1" xml:id="A94624-001-a-3110">Thomas</w>
        <w lemma="Webster" pos="nn1" xml:id="A94624-001-a-3120">Webster</w>
        <pc unit="sentence" xml:id="A94624-001-a-3130">.</pc>
       </item>
       <item xml:id="A94624-e190">
        <w lemma="John" pos="nn1" reg="John" xml:id="A94624-001-a-3140">Iohn</w>
        <w lemma="Richardson" pos="nn1" xml:id="A94624-001-a-3150">Richardson</w>
        <pc unit="sentence" xml:id="A94624-001-a-3160">.</pc>
       </item>
       <item xml:id="A94624-e200">
        <w lemma="John" pos="nn1" reg="John" xml:id="A94624-001-a-3170">Iohn</w>
        <w lemma="dever" pos="nn1" xml:id="A94624-001-a-3180">Dever</w>
        <pc unit="sentence" xml:id="A94624-001-a-3190">.</pc>
       </item>
       <item xml:id="A94624-e210">
        <w lemma="Bryan" pos="nn1" xml:id="A94624-001-a-3200">Bryan</w>
        <w lemma="Lambert" pos="nn1" xml:id="A94624-001-a-3210">Lambert</w>
        <pc unit="sentence" xml:id="A94624-001-a-3220">.</pc>
       </item>
       <item xml:id="A94624-e220">
        <w lemma="james" pos="nn1" reg="James" xml:id="A94624-001-a-3230">Iames</w>
        <w lemma="Gray" pos="nn1" xml:id="A94624-001-a-3240">Gray</w>
        <pc unit="sentence" xml:id="A94624-001-a-3250">.</pc>
       </item>
       <item xml:id="A94624-e230">
        <w lemma="Henry" pos="nn1" xml:id="A94624-001-a-3260">Henry</w>
        <w lemma="Barrow" pos="nn1" xml:id="A94624-001-a-3270">Barrow</w>
        <pc unit="sentence" xml:id="A94624-001-a-3280">.</pc>
       </item>
       <item xml:id="A94624-e240">
        <w lemma="Roger" pos="nn1" xml:id="A94624-001-a-3290">Roger</w>
        <w lemma="Vaughan" pos="nn1" xml:id="A94624-001-a-3300">Vaughan</w>
        <pc unit="sentence" xml:id="A94624-001-a-3310">.</pc>
       </item>
       <item xml:id="A94624-e250">
        <w lemma="Thomas" pos="nn1" xml:id="A94624-001-a-3320">Thomas</w>
        <w lemma="milbourne" pos="nn1" xml:id="A94624-001-a-3330">Milbourne</w>
        <w lemma="senior" pos="fla" xml:id="A94624-001-a-3340">senior</w>
        <pc unit="sentence" xml:id="A94624-001-a-3350">.</pc>
       </item>
      </list>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="A94624-e260">
   <div type="note" xml:id="A94624-e270">
    <p xml:id="A94624-e280">
     <w lemma="for" pos="acp" xml:id="A94624-001-a-3360">For</w>
     <w lemma="themselves" pos="pr" xml:id="A94624-001-a-3370">themselves</w>
     <pc xml:id="A94624-001-a-3380">,</pc>
     <w lemma="and" pos="cc" xml:id="A94624-001-a-3390">and</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-3400">in</w>
     <w lemma="the" pos="d" xml:id="A94624-001-a-3410">the</w>
     <w lemma="name" pos="n2" xml:id="A94624-001-a-3420">names</w>
     <w lemma="of" pos="acp" xml:id="A94624-001-a-3430">of</w>
     <w lemma="150." pos="crd" xml:id="A94624-001-a-3440">150.</w>
     <pc unit="sentence" xml:id="A94624-001-a-3450"/>
     <w lemma="workmen-printer" pos="n2" xml:id="A94624-001-a-3460">Workmen-Printers</w>
     <w lemma="in" pos="acp" xml:id="A94624-001-a-3470">in</w>
     <hi xml:id="A94624-e290">
      <w lemma="London" pos="nn1" xml:id="A94624-001-a-3480">London</w>
      <pc unit="sentence" xml:id="A94624-001-a-3490">.</pc>
     </hi>
    </p>
   </div>
  </back>
 </text>
</TEI>
