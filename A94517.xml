<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A94517">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the King's most excellent Maiesty. The humble address of the Society of the Middle-Temple.</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A94517 of text R185307 in the <ref target="http;//estc.bl.uk">English Short Title Catalog</ref> (Wing T1514). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A94517</idno>
    <idno type="STC">Wing T1514</idno>
    <idno type="STC">ESTC R185307</idno>
    <idno type="EEBO-CITATION">43077687</idno>
    <idno type="OCLC">ocm 43077687</idno>
    <idno type="VID">151799</idno>
    <idno type="PROQUESTGOID">2240881259</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A94517)</note>
    <note>Transcribed from: (Early English Books Online ; image set 151799)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2277:11)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the King's most excellent Maiesty. The humble address of the Society of the Middle-Temple.</title>
      <author>Bernard, John.</author>
      <author>Charles II, King of England, 1630-1685.</author>
      <author>Middle Temple (London, England)</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Printed for Jacob Tonson at the Judges Head in Chancery Lane near Fleetstreet,</publisher>
      <pubPlace>London, :</pubPlace>
      <date>1683.</date>
     </publicationStmt>
     <notesStmt>
      <note>Letter, signed John Bernard, speaker, rejoicing at the delivery from the Rye House plot.</note>
      <note>Reproduction of original in: Newberry Library, Chicago, Illinois.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Charles -- II, -- King of England, 1630-1685.</term>
     <term>Rye House Plot, 1683 -- Sources.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685.</term>
     <term>Broadsides -- London (England) -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>[To the] King's most excellent Maiesty. The humble address of the society of the Middle-Temple.</ep:title>
    <ep:author>Middle Temple </ep:author>
    <ep:publicationYear>1683</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>400</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change><date>2007-06</date><label>TCP</label>
        Assigned for keying and markup
      </change>
   <change><date>2007-06</date><label>Apex CoVantage</label>
        Keyed and coded from ProQuest page images
      </change>
   <change><date>2007-08</date><label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change><date>2007-08</date><label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change><date>2008-02</date><label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A94517-t">
  <body xml:id="A94517-e0">
   <div type="address" xml:id="A94517-e10">
    <pb facs="tcp:151799:1" rend="simple:additions" xml:id="A94517-001-a"/>
    <head xml:id="A94517-e20">
     <w lemma="to" pos="acp" xml:id="A94517-001-a-0010">TO</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0020">THE</w>
     <w lemma="king" pos="ng1" xml:id="A94517-001-a-0030">KING'S</w>
     <w lemma="most" pos="avs-d" xml:id="A94517-001-a-0040">Most</w>
     <w lemma="excellent" pos="j" xml:id="A94517-001-a-0050">Excellent</w>
     <w lemma="majesty" pos="n1" reg="Majesty" xml:id="A94517-001-a-0060">Maiesty</w>
     <pc unit="sentence" xml:id="A94517-001-a-0070">.</pc>
    </head>
    <head type="sub" xml:id="A94517-e30">
     <w lemma="the" pos="d" xml:id="A94517-001-a-0080">The</w>
     <w lemma="humble" pos="j" xml:id="A94517-001-a-0090">Humble</w>
     <w lemma="address" pos="n1" xml:id="A94517-001-a-0100">Address</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-0110">of</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0120">the</w>
     <w lemma="society" pos="n1" xml:id="A94517-001-a-0130">SOCIETY</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-0140">of</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0150">the</w>
     <hi xml:id="A94517-e40">
      <w lemma="Middle-temple" pos="nn1" xml:id="A94517-001-a-0160">Middle-Temple</w>
      <pc unit="sentence" xml:id="A94517-001-a-0170">.</pc>
     </hi>
    </head>
    <opener xml:id="A94517-e50">
     <salute xml:id="A94517-e60">
      <w lemma="dread" pos="j" xml:id="A94517-001-a-0180">DREAD</w>
      <w lemma="sovereign" pos="n1-j" reg="SOVEREIGN" xml:id="A94517-001-a-0190">SOVERAIGN</w>
      <pc xml:id="A94517-001-a-0200">,</pc>
     </salute>
    </opener>
    <p xml:id="A94517-e70">
     <w lemma="with" pos="acp" reg="WITH" xml:id="A94517-001-a-0210">VVITH</w>
     <w lemma="heart" pos="n2" xml:id="A94517-001-a-0220">hearts</w>
     <w lemma="full" pos="j" xml:id="A94517-001-a-0230">full</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-0240">of</w>
     <w lemma="unspeakable" pos="j" xml:id="A94517-001-a-0250">unspeakable</w>
     <w lemma="joy" pos="n1" xml:id="A94517-001-a-0260">Joy</w>
     <w lemma="we" pos="pns" xml:id="A94517-001-a-0270">We</w>
     <w lemma="presume" pos="vvb" xml:id="A94517-001-a-0280">presume</w>
     <w lemma="to" pos="prt" xml:id="A94517-001-a-0290">to</w>
     <w lemma="approach" pos="vvi" xml:id="A94517-001-a-0300">approach</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-0310">your</w>
     <w lemma="royal" pos="j" xml:id="A94517-001-a-0320">Royal</w>
     <w lemma="presence" pos="n1" xml:id="A94517-001-a-0330">presence</w>
     <pc xml:id="A94517-001-a-0340">,</pc>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-0350">and</w>
     <w lemma="with" pos="acp" xml:id="A94517-001-a-0360">with</w>
     <w lemma="all" pos="d" xml:id="A94517-001-a-0370">all</w>
     <w lemma="our" pos="po" xml:id="A94517-001-a-0380">our</w>
     <w lemma="soul" pos="n2" xml:id="A94517-001-a-0390">Souls</w>
     <w lemma="bless" pos="vvb" xml:id="A94517-001-a-0400">bless</w>
     <w lemma="almighty" pos="j" xml:id="A94517-001-a-0410">Almighty</w>
     <w lemma="god" pos="n1" xml:id="A94517-001-a-0420">God</w>
     <w lemma="for" pos="acp" xml:id="A94517-001-a-0430">for</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0440">the</w>
     <w lemma="wonderful" pos="j" xml:id="A94517-001-a-0450">wonderful</w>
     <w lemma="discovery" pos="n1" xml:id="A94517-001-a-0460">discovery</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-0470">of</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0480">the</w>
     <hi xml:id="A94517-e80">
      <w lemma="late" pos="j" xml:id="A94517-001-a-0490">late</w>
      <w lemma="hellish" pos="j" xml:id="A94517-001-a-0500">hellish</w>
      <w lemma="conspiracy" pos="n1" xml:id="A94517-001-a-0510">Conspiracy</w>
     </hi>
     <w lemma="begin" pos="vvn" xml:id="A94517-001-a-0520">begun</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-0530">and</w>
     <w lemma="carry" pos="vvn" xml:id="A94517-001-a-0540">carried</w>
     <w lemma="on" pos="acp" xml:id="A94517-001-a-0550">on</w>
     <w lemma="by" pos="acp" xml:id="A94517-001-a-0560">by</w>
     <hi xml:id="A94517-e90">
      <w lemma="desperate" pos="j" xml:id="A94517-001-a-0570">desperate</w>
      <w lemma="person" pos="n2" xml:id="A94517-001-a-0580">persons</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-0590">of</w>
     <hi xml:id="A94517-e100">
      <w lemma="fanatical" pos="j" xml:id="A94517-001-a-0600">Fanatical</w>
      <pc xml:id="A94517-001-a-0610">,</pc>
      <w lemma="atheistical" pos="j" xml:id="A94517-001-a-0620">Atheistical</w>
      <w lemma="and" pos="cc" xml:id="A94517-001-a-0630">and</w>
      <w lemma="republican" pos="n1" xml:id="A94517-001-a-0640">Republican</w>
      <w lemma="principle" pos="n2" xml:id="A94517-001-a-0650">Principles</w>
      <pc xml:id="A94517-001-a-0660">,</pc>
     </hi>
     <w lemma="who" pos="crq" xml:id="A94517-001-a-0670">who</w>
     <w lemma="impudent" pos="av-j" xml:id="A94517-001-a-0680">impudently</w>
     <w lemma="assume" pos="vvg" xml:id="A94517-001-a-0690">assuming</w>
     <w lemma="to" pos="acp" xml:id="A94517-001-a-0700">to</w>
     <w lemma="themselves" pos="pr" xml:id="A94517-001-a-0710">themselves</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0720">the</w>
     <w lemma="name" pos="n1" xml:id="A94517-001-a-0730">name</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-0740">of</w>
     <hi xml:id="A94517-e110">
      <w lemma="true" pos="j" xml:id="A94517-001-a-0750">True</w>
      <w lemma="protestant" pos="nn2" xml:id="A94517-001-a-0760">Protestants</w>
      <w lemma="and" pos="cc" xml:id="A94517-001-a-0770">and</w>
      <w lemma="patriot" pos="n2" xml:id="A94517-001-a-0780">Patriots</w>
      <pc xml:id="A94517-001-a-0790">,</pc>
     </hi>
     <w lemma="do" pos="vvd" xml:id="A94517-001-a-0800">did</w>
     <w lemma="at" pos="acp" xml:id="A94517-001-a-0810">at</w>
     <w lemma="first" pos="ord" xml:id="A94517-001-a-0820">first</w>
     <w lemma="by" pos="acp" xml:id="A94517-001-a-0830">by</w>
     <w lemma="popular" pos="j" xml:id="A94517-001-a-0840">Popular</w>
     <w lemma="insinuation" pos="n2" xml:id="A94517-001-a-0850">Insinuations</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-0860">and</w>
     <w lemma="other" pos="d" xml:id="A94517-001-a-0870">other</w>
     <w lemma="artifices" pos="fla" xml:id="A94517-001-a-0880">Artifices</w>
     <pc xml:id="A94517-001-a-0890">,</pc>
     <w lemma="project" pos="n1" xml:id="A94517-001-a-0900">project</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0910">the</w>
     <w lemma="undermine" pos="j-vg" xml:id="A94517-001-a-0920">undermining</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0930">the</w>
     <w lemma="best" pos="js" xml:id="A94517-001-a-0940">best</w>
     <w lemma="religion" pos="n1" xml:id="A94517-001-a-0950">Religion</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-0960">and</w>
     <w lemma="government" pos="n1" xml:id="A94517-001-a-0970">Government</w>
     <w lemma="in" pos="acp" xml:id="A94517-001-a-0980">in</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-0990">the</w>
     <w lemma="world" pos="n1" xml:id="A94517-001-a-1000">World</w>
     <pc xml:id="A94517-001-a-1010">;</pc>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-1020">and</w>
     <w lemma="afterward" pos="av" xml:id="A94517-001-a-1030">afterwards</w>
     <w lemma="be" pos="vvg" xml:id="A94517-001-a-1040">being</w>
     <w lemma="therein" pos="av" xml:id="A94517-001-a-1050">therein</w>
     <w lemma="prevent" pos="vvn" xml:id="A94517-001-a-1060">prevented</w>
     <hi xml:id="A94517-e120">
      <w lemma="by" pos="acp" xml:id="A94517-001-a-1070">by</w>
      <w lemma="your" pos="po" xml:id="A94517-001-a-1080">your</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="A94517-001-a-1090">Majesties</w>
      <w lemma="unwearied" pos="j" xml:id="A94517-001-a-1100">unwearied</w>
      <w lemma="care" pos="n1" xml:id="A94517-001-a-1110">Care</w>
      <w lemma="and" pos="cc" xml:id="A94517-001-a-1120">and</w>
      <w lemma="admirable" pos="j" xml:id="A94517-001-a-1130">admirable</w>
      <w lemma="conduct" pos="n1" xml:id="A94517-001-a-1140">Conduct</w>
      <pc xml:id="A94517-001-a-1150">,</pc>
     </hi>
     <w lemma="proceed" pos="vvd" xml:id="A94517-001-a-1160">proceeded</w>
     <w lemma="to" pos="prt" xml:id="A94517-001-a-1170">to</w>
     <w lemma="contrive" pos="vvi" xml:id="A94517-001-a-1180">contrive</w>
     <hi xml:id="A94517-e130">
      <w lemma="the" pos="d" xml:id="A94517-001-a-1190">the</w>
      <w lemma="horrid" pos="j" xml:id="A94517-001-a-1200">horrid</w>
      <w lemma="parricide" pos="n1" xml:id="A94517-001-a-1210">Parricide</w>
      <w lemma="of" pos="acp" xml:id="A94517-001-a-1220">of</w>
      <w lemma="your" pos="po" xml:id="A94517-001-a-1230">your</w>
      <w lemma="sacred" pos="j" xml:id="A94517-001-a-1240">Sacred</w>
      <w lemma="person" pos="n1" xml:id="A94517-001-a-1250">Person</w>
      <pc xml:id="A94517-001-a-1260">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="A94517-001-a-1270">the</w>
     <w lemma="barbarous" pos="j" xml:id="A94517-001-a-1280">barbarous</w>
     <w lemma="assassination" pos="n1" xml:id="A94517-001-a-1290">Assassination</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-1300">of</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-1310">your</w>
     <w lemma="royal" pos="j" xml:id="A94517-001-a-1320">Royal</w>
     <w lemma="brother" pos="n1" xml:id="A94517-001-a-1330">Brother</w>
     <pc xml:id="A94517-001-a-1340">,</pc>
     <w lemma="the" pos="d" xml:id="A94517-001-a-1350">the</w>
     <w lemma="dear" pos="j" xml:id="A94517-001-a-1360">dear</w>
     <w lemma="partaker" pos="n1" xml:id="A94517-001-a-1370">partaker</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-1380">of</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-1390">your</w>
     <w lemma="suffering" pos="n2" xml:id="A94517-001-a-1400">Sufferings</w>
     <pc xml:id="A94517-001-a-1410">,</pc>
     <w lemma="the" pos="d" xml:id="A94517-001-a-1420">the</w>
     <w lemma="involve" pos="j-vg" xml:id="A94517-001-a-1430">involving</w>
     <w lemma="these" pos="d" xml:id="A94517-001-a-1440">these</w>
     <w lemma="nation" pos="n2" xml:id="A94517-001-a-1450">Nations</w>
     <w lemma="in" pos="acp" xml:id="A94517-001-a-1460">in</w>
     <w lemma="blood" pos="n1" xml:id="A94517-001-a-1470">Blood</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-1480">and</w>
     <w lemma="confusion" pos="n1" xml:id="A94517-001-a-1490">Confusion</w>
     <pc xml:id="A94517-001-a-1500">,</pc>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-1510">and</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-1520">the</w>
     <w lemma="utter" pos="j" xml:id="A94517-001-a-1530">utter</w>
     <w lemma="destruction" pos="n1" xml:id="A94517-001-a-1540">destruction</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-1550">of</w>
     <w lemma="this" pos="d" xml:id="A94517-001-a-1560">this</w>
     <w lemma="monarchy" pos="n1" xml:id="A94517-001-a-1570">Monarchy</w>
     <pc unit="sentence" xml:id="A94517-001-a-1580">.</pc>
    </p>
    <p xml:id="A94517-e140">
     <w lemma="as" pos="acp" xml:id="A94517-001-a-1590">As</w>
     <w lemma="this" pos="d" xml:id="A94517-001-a-1600">this</w>
     <w lemma="society" pos="n1" xml:id="A94517-001-a-1610">Society</w>
     <w lemma="have" pos="vvz" xml:id="A94517-001-a-1620">has</w>
     <w lemma="be" pos="vvn" xml:id="A94517-001-a-1630">been</w>
     <w lemma="eminent" pos="j" xml:id="A94517-001-a-1640">eminent</w>
     <w lemma="for" pos="acp" xml:id="A94517-001-a-1650">for</w>
     <w lemma="its" pos="po" xml:id="A94517-001-a-1660">its</w>
     <w lemma="loyalty" pos="n1" xml:id="A94517-001-a-1670">Loyalty</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-1680">and</w>
     <w lemma="early" pos="j" xml:id="A94517-001-a-1690">early</w>
     <w lemma="token" pos="n2" xml:id="A94517-001-a-1700">tokens</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-1710">of</w>
     <w lemma="duty" pos="n1" xml:id="A94517-001-a-1720">Duty</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-1730">and</w>
     <w lemma="affection" pos="n1" xml:id="A94517-001-a-1740">Affection</w>
     <pc xml:id="A94517-001-a-1750">,</pc>
     <w lemma="particular" pos="av-j" xml:id="A94517-001-a-1760">particularly</w>
     <w lemma="in" pos="acp" xml:id="A94517-001-a-1770">in</w>
     <w lemma="their" pos="po" xml:id="A94517-001-a-1780">their</w>
     <w lemma="humble" pos="j" xml:id="A94517-001-a-1790">humble</w>
     <w lemma="thanks" pos="n2" xml:id="A94517-001-a-1800">thanks</w>
     <w lemma="for" pos="acp" xml:id="A94517-001-a-1810">for</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-1820">your</w>
     <w lemma="gracious" pos="j" xml:id="A94517-001-a-1830">Gracious</w>
     <w lemma="declaration" pos="n1" xml:id="A94517-001-a-1840">Declaration</w>
     <pc xml:id="A94517-001-a-1850">,</pc>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-1860">and</w>
     <w lemma="their" pos="po" xml:id="A94517-001-a-1870">their</w>
     <w lemma="abhorrence" pos="n1" xml:id="A94517-001-a-1880">Abhorrence</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-1890">of</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-1900">the</w>
     <w lemma="late" pos="j" xml:id="A94517-001-a-1910">late</w>
     <hi xml:id="A94517-e150">
      <w lemma="accurse" pos="j-vn" xml:id="A94517-001-a-1920">Accursed</w>
      <w lemma="and" pos="cc" xml:id="A94517-001-a-1930">and</w>
      <w lemma="traitorous" pos="j" reg="Traitorous" xml:id="A94517-001-a-1940">Traiterous</w>
      <w lemma="association" pos="n1" xml:id="A94517-001-a-1950">Association</w>
      <pc xml:id="A94517-001-a-1960">,</pc>
     </hi>
     <w lemma="which" pos="crq" xml:id="A94517-001-a-1970">which</w>
     <w lemma="we" pos="pns" xml:id="A94517-001-a-1980">we</w>
     <w lemma="look" pos="vvb" xml:id="A94517-001-a-1990">look</w>
     <w lemma="upon" pos="acp" xml:id="A94517-001-a-2000">upon</w>
     <w lemma="to" pos="prt" xml:id="A94517-001-a-2010">to</w>
     <w lemma="be" pos="vvi" xml:id="A94517-001-a-2020">be</w>
     <w lemma="a" pos="d" xml:id="A94517-001-a-2030">a</w>
     <w lemma="part" pos="n1" xml:id="A94517-001-a-2040">part</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-2050">of</w>
     <w lemma="this" pos="d" xml:id="A94517-001-a-2060">this</w>
     <hi xml:id="A94517-e160">
      <w lemma="damnable" pos="j" xml:id="A94517-001-a-2070">Damnable</w>
      <w lemma="conspiracy" pos="n1" xml:id="A94517-001-a-2080">Conspiracy</w>
      <pc xml:id="A94517-001-a-2090">,</pc>
     </hi>
     <w lemma="so" pos="av" xml:id="A94517-001-a-2100">so</w>
     <w lemma="we" pos="pns" xml:id="A94517-001-a-2110">we</w>
     <w lemma="shall" pos="vmb" xml:id="A94517-001-a-2120">shall</w>
     <w lemma="do" pos="vvi" xml:id="A94517-001-a-2130">do</w>
     <w lemma="our" pos="po" xml:id="A94517-001-a-2140">our</w>
     <w lemma="utmost" pos="j" xml:id="A94517-001-a-2150">utmost</w>
     <w lemma="to" pos="prt" xml:id="A94517-001-a-2160">to</w>
     <w lemma="bring" pos="vvi" xml:id="A94517-001-a-2170">bring</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-2180">the</w>
     <hi xml:id="A94517-e170">
      <w lemma="villain" pos="n2" xml:id="A94517-001-a-2190">Villains</w>
     </hi>
     <w lemma="to" pos="acp" xml:id="A94517-001-a-2200">to</w>
     <w lemma="justice" pos="n1" xml:id="A94517-001-a-2210">Justice</w>
     <pc xml:id="A94517-001-a-2220">,</pc>
     <w lemma="especial" pos="av-j" xml:id="A94517-001-a-2230">especially</w>
     <w lemma="those" pos="d" xml:id="A94517-001-a-2240">those</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-2250">of</w>
     <w lemma="this" pos="d" xml:id="A94517-001-a-2260">this</w>
     <w lemma="society" pos="n1" xml:id="A94517-001-a-2270">Society</w>
     <pc xml:id="A94517-001-a-2280">,</pc>
     <w lemma="who" pos="crq" xml:id="A94517-001-a-2290">who</w>
     <w lemma="to" pos="acp" xml:id="A94517-001-a-2300">to</w>
     <w lemma="our" pos="po" xml:id="A94517-001-a-2310">our</w>
     <w lemma="great" pos="j" xml:id="A94517-001-a-2320">great</w>
     <w lemma="sorrow" pos="n1" xml:id="A94517-001-a-2330">Sorrow</w>
     <w lemma="be" pos="vvb" xml:id="A94517-001-a-2340">are</w>
     <w lemma="in" pos="acp" xml:id="A94517-001-a-2350">in</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-2360">the</w>
     <w lemma="number" pos="n1" xml:id="A94517-001-a-2370">number</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-2380">of</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-2390">the</w>
     <w lemma="conspirator" pos="n2" xml:id="A94517-001-a-2400">Conspirators</w>
     <pc unit="sentence" xml:id="A94517-001-a-2410">.</pc>
    </p>
    <p xml:id="A94517-e180">
     <w lemma="and" pos="cc" xml:id="A94517-001-a-2420">And</w>
     <w lemma="we" pos="pns" xml:id="A94517-001-a-2430">We</w>
     <w lemma="do" pos="vvb" xml:id="A94517-001-a-2440">do</w>
     <w lemma="repeat" pos="vvi" xml:id="A94517-001-a-2450">repeat</w>
     <w lemma="our" pos="po" xml:id="A94517-001-a-2460">our</w>
     <w lemma="solemn" pos="j" xml:id="A94517-001-a-2470">solemn</w>
     <w lemma="protestation" pos="n2" xml:id="A94517-001-a-2480">Protestations</w>
     <w lemma="to" pos="prt" xml:id="A94517-001-a-2490">to</w>
     <w lemma="stand" pos="vvi" xml:id="A94517-001-a-2500">stand</w>
     <w lemma="by" pos="acp" xml:id="A94517-001-a-2510">by</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-2520">and</w>
     <w lemma="defend" pos="vvi" xml:id="A94517-001-a-2530">defend</w>
     <hi xml:id="A94517-e190">
      <w lemma="your" pos="po" xml:id="A94517-001-a-2540">Your</w>
      <w lemma="sacred" pos="j" xml:id="A94517-001-a-2550">Sacred</w>
      <w lemma="majesty" pos="n1" xml:id="A94517-001-a-2560">Majesty</w>
      <w lemma="and" pos="cc" xml:id="A94517-001-a-2570">and</w>
      <w lemma="lawful" pos="j" xml:id="A94517-001-a-2580">lawful</w>
      <w lemma="successor" pos="n2" xml:id="A94517-001-a-2590">Successors</w>
     </hi>
     <w lemma="with" pos="acp" xml:id="A94517-001-a-2600">with</w>
     <w lemma="our" pos="po" xml:id="A94517-001-a-2610">our</w>
     <w lemma="life" pos="n2" xml:id="A94517-001-a-2620">Lives</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-2630">and</w>
     <w lemma="fortune" pos="n2" xml:id="A94517-001-a-2640">Fortunes</w>
     <pc xml:id="A94517-001-a-2650">,</pc>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-2660">and</w>
     <w lemma="beseech" pos="vvb" xml:id="A94517-001-a-2670">beseech</w>
     <w lemma="almighty" pos="j" xml:id="A94517-001-a-2680">Almighty</w>
     <w lemma="God" pos="nn1" xml:id="A94517-001-a-2690">God</w>
     <w lemma="to" pos="prt" xml:id="A94517-001-a-2700">to</w>
     <w lemma="cover" pos="vvi" xml:id="A94517-001-a-2710">cover</w>
     <w lemma="with" pos="acp" xml:id="A94517-001-a-2720">with</w>
     <w lemma="confusion" pos="n1" xml:id="A94517-001-a-2730">confusion</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-2740">the</w>
     <w lemma="face" pos="n2" xml:id="A94517-001-a-2750">faces</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-2760">of</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-2770">Your</w>
     <w lemma="most" pos="avs-d" xml:id="A94517-001-a-2780">most</w>
     <w lemma="secret" pos="j" xml:id="A94517-001-a-2790">secret</w>
     <w lemma="enemy" pos="n2" xml:id="A94517-001-a-2800">Enemies</w>
     <pc xml:id="A94517-001-a-2810">,</pc>
     <w lemma="that" pos="cs" xml:id="A94517-001-a-2820">that</w>
     <hi xml:id="A94517-e200">
      <w lemma="divine" pos="j" xml:id="A94517-001-a-2830">Divine</w>
      <w lemma="vengeance" pos="n1" xml:id="A94517-001-a-2840">Vengeance</w>
     </hi>
     <w lemma="may" pos="vmb" xml:id="A94517-001-a-2850">may</w>
     <w lemma="overtake" pos="vvi" xml:id="A94517-001-a-2860">overtake</w>
     <w lemma="such" pos="d" xml:id="A94517-001-a-2870">such</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-2880">of</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-2890">the</w>
     <hi xml:id="A94517-e210">
      <w lemma="traitor" pos="n2" xml:id="A94517-001-a-2900">Traitors</w>
     </hi>
     <w lemma="as" pos="acp" xml:id="A94517-001-a-2910">as</w>
     <w lemma="by" pos="acp" xml:id="A94517-001-a-2920">by</w>
     <w lemma="flight" pos="n1" xml:id="A94517-001-a-2930">flight</w>
     <w lemma="escape" pos="vvi" xml:id="A94517-001-a-2940">escape</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-2950">the</w>
     <w lemma="justice" pos="n1" xml:id="A94517-001-a-2960">Justice</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-2970">of</w>
     <hi xml:id="A94517-e220">
      <w lemma="humane" pos="j" xml:id="A94517-001-a-2980">Humane</w>
      <w lemma="law" pos="n2" xml:id="A94517-001-a-2990">Laws</w>
      <pc xml:id="A94517-001-a-3000">,</pc>
     </hi>
     <w lemma="who" pos="crq" xml:id="A94517-001-a-3010">whose</w>
     <w lemma="gild" pos="n1" reg="Gild" xml:id="A94517-001-a-3020">Guilt</w>
     <w lemma="proclaim" pos="vvz" xml:id="A94517-001-a-3030">proclaims</w>
     <w lemma="itself" pos="pr" reg="itself" xml:id="A94517-001-a-3040">it self</w>
     <w lemma="so" pos="av" xml:id="A94517-001-a-3060">so</w>
     <w lemma="loud" pos="av-j" xml:id="A94517-001-a-3070">loud</w>
     <pc xml:id="A94517-001-a-3080">,</pc>
     <w lemma="that" pos="cs" xml:id="A94517-001-a-3090">that</w>
     <w lemma="they" pos="pns" xml:id="A94517-001-a-3100">they</w>
     <w lemma="dare" pos="vvb" xml:id="A94517-001-a-3110">dare</w>
     <w lemma="not" pos="xx" xml:id="A94517-001-a-3120">not</w>
     <w lemma="trust" pos="vvi" xml:id="A94517-001-a-3130">trust</w>
     <w lemma="even" pos="av" xml:id="A94517-001-a-3140">even</w>
     <w lemma="that" pos="d" xml:id="A94517-001-a-3150">that</w>
     <hi xml:id="A94517-e230">
      <w lemma="mercy" pos="n1" xml:id="A94517-001-a-3160">Mercy</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-3170">of</w>
     <w lemma="which" pos="crq" xml:id="A94517-001-a-3180">which</w>
     <w lemma="they" pos="pns" xml:id="A94517-001-a-3190">they</w>
     <w lemma="have" pos="vvb" xml:id="A94517-001-a-3200">have</w>
     <w lemma="have" pos="vvn" xml:id="A94517-001-a-3210">had</w>
     <w lemma="so" pos="av" xml:id="A94517-001-a-3220">so</w>
     <w lemma="long" pos="j" xml:id="A94517-001-a-3230">long</w>
     <w lemma="experience" pos="n1" xml:id="A94517-001-a-3240">experience</w>
     <pc unit="sentence" xml:id="A94517-001-a-3250">.</pc>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-3260">And</w>
     <w lemma="as" pos="acp" xml:id="A94517-001-a-3270">as</w>
     <w lemma="providence" pos="n1" xml:id="A94517-001-a-3280">Providence</w>
     <w lemma="do" pos="vvd" xml:id="A94517-001-a-3290">did</w>
     <w lemma="never" pos="avx" xml:id="A94517-001-a-3300">never</w>
     <w lemma="so" pos="av" xml:id="A94517-001-a-3310">so</w>
     <w lemma="signalise" pos="vvi" reg="signalise" xml:id="A94517-001-a-3320">signalize</w>
     <w lemma="itself" pos="pr" reg="itself" xml:id="A94517-001-a-3330">it self</w>
     <w lemma="on" pos="acp" xml:id="A94517-001-a-3350">on</w>
     <w lemma="behalf" pos="n1" xml:id="A94517-001-a-3360">behalf</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-3370">of</w>
     <w lemma="any" pos="d" xml:id="A94517-001-a-3380">any</w>
     <w lemma="prince" pos="n1" xml:id="A94517-001-a-3390">Prince</w>
     <w lemma="as" pos="acp" xml:id="A94517-001-a-3400">as</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-3410">of</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-3420">your</w>
     <w lemma="majesty" pos="n1" xml:id="A94517-001-a-3430">Majesty</w>
     <w lemma="through" pos="acp" xml:id="A94517-001-a-3440">through</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-3450">the</w>
     <w lemma="whole" pos="j" xml:id="A94517-001-a-3460">whole</w>
     <w lemma="course" pos="n1" xml:id="A94517-001-a-3470">course</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-3480">of</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-3490">Your</w>
     <w lemma="reign" pos="n1" xml:id="A94517-001-a-3500">Reign</w>
     <pc xml:id="A94517-001-a-3510">,</pc>
     <w lemma="so" pos="av" xml:id="A94517-001-a-3520">so</w>
     <w lemma="may" pos="vmb" xml:id="A94517-001-a-3530">may</w>
     <w lemma="heaven" pos="n1" xml:id="A94517-001-a-3540">Heaven</w>
     <w lemma="shower" pos="n1" reg="shower" xml:id="A94517-001-a-3550">showr</w>
     <w lemma="down" pos="acp" xml:id="A94517-001-a-3560">down</w>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-3570">and</w>
     <w lemma="continue" pos="vvi" xml:id="A94517-001-a-3580">continue</w>
     <w lemma="its" pos="po" xml:id="A94517-001-a-3590">its</w>
     <w lemma="best" pos="js" xml:id="A94517-001-a-3600">Best</w>
     <w lemma="blessing" pos="n2" xml:id="A94517-001-a-3610">Blessings</w>
     <hi xml:id="A94517-e240">
      <w lemma="on" pos="acp" xml:id="A94517-001-a-3620">on</w>
      <w lemma="the" pos="d" xml:id="A94517-001-a-3630">the</w>
      <w lemma="best" pos="js" xml:id="A94517-001-a-3640">Best</w>
      <w lemma="of" pos="acp" xml:id="A94517-001-a-3650">of</w>
      <w lemma="king" pos="n2" xml:id="A94517-001-a-3660">Kings</w>
      <pc xml:id="A94517-001-a-3670">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="A94517-001-a-3680">and</w>
     <w lemma="be" pos="vvi" xml:id="A94517-001-a-3690">be</w>
     <w lemma="never" pos="avx" xml:id="A94517-001-a-3700">never</w>
     <w lemma="weary" pos="j" xml:id="A94517-001-a-3710">weary</w>
     <w lemma="of" pos="acp" xml:id="A94517-001-a-3720">of</w>
     <w lemma="work" pos="vvg" xml:id="A94517-001-a-3730">working</w>
     <w lemma="new" pos="j" xml:id="A94517-001-a-3740">new</w>
     <w lemma="miracle" pos="n2" xml:id="A94517-001-a-3750">Miracles</w>
     <w lemma="for" pos="acp" xml:id="A94517-001-a-3760">for</w>
     <w lemma="your" pos="po" xml:id="A94517-001-a-3770">Your</w>
     <w lemma="preservation" pos="n1" xml:id="A94517-001-a-3780">preservation</w>
     <pc unit="sentence" xml:id="A94517-001-a-3790">.</pc>
    </p>
    <closer xml:id="A94517-e250">
     <signed xml:id="A94517-e260">
      <w lemma="John" pos="nn1" xml:id="A94517-001-a-3800">John</w>
      <w lemma="Bernard" pos="nn1" xml:id="A94517-001-a-3810">Bernard</w>
      <pc xml:id="A94517-001-a-3820">,</pc>
     </signed>
     <w lemma="speaker" pos="n1" xml:id="A94517-001-a-3830">Speaker</w>
     <pc unit="sentence" xml:id="A94517-001-a-3840">.</pc>
    </closer>
   </div>
  </body>
  <back xml:id="A94517-e270">
   <div type="colophon" xml:id="A94517-e280">
    <p xml:id="A94517-e290">
     <hi xml:id="A94517-e300">
      <w lemma="LONDON" pos="nn1" xml:id="A94517-001-a-3850">LONDON</w>
      <pc xml:id="A94517-001-a-3860">,</pc>
     </hi>
     <w lemma="print" pos="vvn" xml:id="A94517-001-a-3870">Printed</w>
     <w lemma="for" pos="acp" xml:id="A94517-001-a-3880">for</w>
     <hi xml:id="A94517-e310">
      <w lemma="Jacob" pos="nn1" xml:id="A94517-001-a-3890">Jacob</w>
      <w lemma="tonson" pos="nn1" xml:id="A94517-001-a-3900">Tonson</w>
     </hi>
     <w lemma="at" pos="acp" xml:id="A94517-001-a-3910">at</w>
     <w lemma="the" pos="d" xml:id="A94517-001-a-3920">the</w>
     <w lemma="judge" pos="ng1" reg="Judge's" xml:id="A94517-001-a-3930">Judges</w>
     <w lemma="head" pos="n1" xml:id="A94517-001-a-3940">Head</w>
     <w lemma="in" pos="acp" xml:id="A94517-001-a-3950">in</w>
     <hi xml:id="A94517-e320">
      <w lemma="chancery" pos="n1" xml:id="A94517-001-a-3960">Chancery</w>
      <w lemma="lane" pos="n1" xml:id="A94517-001-a-3970">Lane</w>
     </hi>
     <w lemma="near" pos="acp" xml:id="A94517-001-a-3980">near</w>
     <hi xml:id="A94517-e330">
      <w lemma="Fleetstreet" pos="nn1" xml:id="A94517-001-a-3990">Fleetstreet</w>
      <pc xml:id="A94517-001-a-4000">,</pc>
     </hi>
     <w lemma="1683." pos="crd" xml:id="A94517-001-a-4010">1683.</w>
     <pc unit="sentence" xml:id="A94517-001-a-4020"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
